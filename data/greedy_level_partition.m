%%%% queue %%%%
% baseline = [915936,30144172,198827019.5];
% baseline_std = [93498,5557412,13168902];
% optimal = [416561 3851657 21367131];
% optimal_std = [118065 629672 2682178];
% 
% greedy_search = [236618 1293980 1511500];
% greedy_full = [465744 8307233 28211130];
% greedy_std = [66629 531291 5807064];
% 
% empty = [0 0 0];

%%%% cpp %%%%
% baseline = [955242 13177112 195773797];
% baseline_std = [97162 1524548 14617540];
% optimal = [562210 2891827 18438095];
% optimal_std = [80508 336792 3400349];
% 
% greedy_search = [170031 1294507 2263226];
% greedy_full = [513897 5379540 48457578];
% greedy_std = [39956 528364 3917788];
% 

% group_bar = cat(1, baseline, optimal, empty);
% group_bar_error = cat(1, baseline_std, optimal_std, greedy_std, empty);
% stacked_bar = cat(1, greedy_full, greedy_search);
% 
% all_grouped_bar = cat(1, baseline, optimal, greedy_full, greedy_search);
% scaled_grouped_bar = bsxfun(@rdivide, all_grouped_bar,baseline);
% scaled_bar_error = bsxfun(@rdivide, group_bar_error, [955242 13177112 195773797]);
% 
% figure;
% hold on
% % set(gca, 'YScale', 'log')
% % bar(group_bar.');
% bar(scaled_grouped_bar.');
% ngroups = 3;
% nbars = 4;
% width = min(0.8, nbars/(nbars + 1.5));
% % greedy_x = [0 0 0];
% for i = 1:nbars
%     % Calculate center of each bar
%     x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
%     % errorbar(x, scaled_grouped_bar(i,:), scaled_bar_error(i,:), 'k', 'linestyle', 'none');
%     % greedy_x = x;
%     if i == 1
%         text(x, [1.02 1.02 1.02], string(baseline));
%     end
% end
% % full = bar(greedy_x, greedy_search+greedy_full, width/3.6, 'r');
% % errorbar(greedy_x, greedy_search+greedy_full, greedy_std, 'linestyle', 'none');
% % uistack(full,'bottom')
% % bar(greedy_x, stacked_bar.', width/3.6, 'stacked');
% set(gca, 'XTick', [1 2 3])
% set(gca, 'XTickLabel', {'Small' 'Tiny', 'Rare'})
% xlabel('query type')
% ylabel('total number of simulation steps')
% hold off

%%%% nn %%%%
baseline = [1009431, 7262735];
optimal = [196913 804035];

greedy_search = [74137 186600];
empty = [0,0];
greedy_full = [372311 814035];

group_bar = cat(1, baseline, optimal, empty);
stacked_bar = cat(1, greedy_full, greedy_search);
all_group_bar = cat(1, baseline, optimal, greedy_full, greedy_search);
scaled_all_group_bar = bsxfun(@rdivide, all_group_bar, baseline);

figure;
hold on
% set(gca, 'YScale', 'log')
% bar(group_bar.');
bar(scaled_all_group_bar.');
ngroups = 2;
nbars = 3;
width = min(0.8, nbars/(nbars + 1.5));
greedy_x = [0 0 0];
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
    % greedy_x = x;
    if i == 1
        text(x, [1.02 1.02], string(baseline));
    end
end
% full = bar(greedy_x, greedy_search+greedy_full, width/3.6, 'r');
% uistack(full,'bottom')
% bar(greedy_x, stacked_bar.',width/3.6, 'stacked');
set(gca, 'XTick', [1 2])
set(gca, 'XTickLabel', {'Small' 'Tiny'})
xlabel('query type')
ylabel('total number of simulation steps')
hold off

