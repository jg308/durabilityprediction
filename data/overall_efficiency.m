queue_srs_s = [2601506,915936,30144172,198827019.5];
queue_srs_s_std = [59925,93498,5557412,13168902];
queue_mlss_s = [1843449,446084,3851657,21367131];
queue_mlss_s_std = [60364,67693,629672,2682178];

queue_step = cat(1, queue_srs_s, queue_mlss_s);
queue_step_error = cat(1, queue_srs_s_std, queue_mlss_s_std);

queue_srs_t = [9.218702023029328,3.248079631328583,104.93139758110047,668.8570769548417];
queue_srs_t_std = [0.2984506947424043,0.3294989642952145,19.6918560901173,47.13779561582241];
queue_mlss_t = [6.693851498266061,1.6151598704229926,13.534512686729432,71.94501585960388];
queue_mlss_t_std = [0.2664197717154943,0.25019522821448914,2.2903079613866635,8.34389183258993];

queue_time = cat(1, queue_srs_t, queue_mlss_t);
queue_time_error = cat(1, queue_srs_t_std, queue_mlss_t_std);

% figure;
% bar(queue_step.', 'grouped');
% hold on
% % Find the number of groups and the number of bars in each group
% ngroups = 4;
% nbars = 2;
% width = min(0.8, nbars/(nbars + 1.5));
% for i = 1:nbars
%     % Calculate center of each bar
%     x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
%     errorbar(x, queue_step(i,:), queue_step_error(i,:), 'k', 'linestyle', 'none');
% end
% hold off

% figure;
% bar(queue_time.', 'grouped');
% hold on
% % Find the number of groups and the number of bars in each group
% ngroups = 4;
% nbars = 2;
% width = min(0.8, nbars/(nbars + 1.5));
% for i = 1:nbars
%     % Calculate center of each bar
%     x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
%     errorbar(x, queue_time(i,:), queue_time_error(i,:), 'k', 'linestyle', 'none');
% end
% hold off

cpp_srs_s = [2460570 955242 13177112 195773797];
cpp_srs_s_std = [68050 97162 1524548 14617540];
cpp_mlss_s = [1945843 562210 2891827 18438095];
cpp_mlss_s_std = [78298 80508 336792 3400349];

cpp_step = cat(1, cpp_srs_s, cpp_mlss_s);
cpp_step_error = cat(1, cpp_srs_s_std, cpp_mlss_s_std);

cpp_srs_t = [6.555569338798523 2.396479787826538 34.28239357471466 533.5123735666275];
cpp_srs_t_std = [0.20599712692679686 0.23941833706799587 4.003653464211334 36.09198347176298];
cpp_mlss_t = [5.212433870215165 1.4125615161853833 7.581152942445543 49.84995036125183];
cpp_mlss_t_std = [0.2195599799580642 0.20601349844115793 0.8939114181755695 8.896983971096766];

cpp_time = cat(1, cpp_srs_t, cpp_mlss_t);
cpp_time_error = cat(1, cpp_srs_t_std, cpp_mlss_t_std);

% figure;
% bar(cpp_time.', 'grouped');
% hold on
% % Find the number of groups and the number of bars in each group
% ngroups = 4;
% nbars = 2;
% width = min(0.8, nbars/(nbars + 1.5));
% for i = 1:nbars
%     % Calculate center of each bar
%     x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
%     errorbar(x, cpp_time(i,:), cpp_time_error(i,:), 'k', 'linestyle', 'none');
% end
% hold off

figure;
bar(cpp_step.', 'grouped');
hold on
% Find the number of groups and the number of bars in each group
ngroups = 4;
nbars = 2;
width = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - width/2 + (2*i-1) * width / (2*nbars);
    errorbar(x, cpp_step(i,:), cpp_step_error(i,:), 'k', 'linestyle', 'none');
end
hold off

