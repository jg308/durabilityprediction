from matplotlib import pyplot as plt
import matplotlib
import json
import argparse
import numpy as np

font = {'family' : 'normal',
        'size'   : 16}

matplotlib.rc('font', **font)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('srs_data', type=str, help='json file for srs data')
	parser.add_argument('mlss_data', type=str, help='json file for mlss data')
	# parser.add_argument('seed', type=int, help='a random number')
	parser.add_argument('fig_type', type=str, help='figure type')

	args = parser.parse_args()

	with open(args.srs_data, 'r') as f:
		srs_data = json.load(f)
	with open(args.mlss_data, 'r') as f:
		mlss_data = json.load(f)

	if args.fig_type == 're':
		step = 2
		srs_trace = [item[3] for item in srs_data[step::step]]
		srs_estimate = [item[0] for item in srs_data[step::step]]
		srs_error = [item[1] for item in srs_data[step::step]]

		mlss_trace = [item[3] for item in mlss_data[step::step]]
		mlss_estimate = [item[0] for item in mlss_data[step::step]]
		mlss_error = [item[1] for item in mlss_data[step::step]]

		plt.plot([min(srs_trace[0]/3600, mlss_trace[0]/3600), srs_trace[-1]/3600],[0.1,0.1],'--', linewidth=2,\
			color='black', label='threshold')
		plt.plot(np.array(srs_trace)/3600, srs_error, '-*', color='b', label='SRS')
		plt.plot(np.array(mlss_trace)/3600, mlss_error, '-o', color='r', label='MLSS')

		# plt.xlabel('second')
		plt.xlabel('hour')
		plt.ylabel('relative error (%)')
		plt.legend()
		plt.ylim([0, 2])
		plt.legend(prop={'size': 16})
		plt.tight_layout()
		plt.show()

	if args.fig_type == 'ci':

		step = 5
		srs_start_idx = 0
		mlss_start_idx = 0

		for i,v in enumerate([item[1] for item in srs_data]):
			if v > 0:
				srs_start_idx = i
				break
		for i,v in enumerate([item[1] for item in mlss_data]):
			if v > 0:
				mlss_start_idx = i
				break

		ground_truth = srs_data[-1][0]
		ci = srs_data[-1][1]

		print(len(srs_data[srs_start_idx:]))
		print(mlss_data)

		srs_trace = [item[3] for item in srs_data[srs_start_idx:]]
		srs_error = np.array([item[1] for item in srs_data[srs_start_idx:]])/ground_truth
		srs_estimate = np.array([(item[0]-ground_truth)/ground_truth for item in srs_data[srs_start_idx:]])

		mlss_trace = [item[3] for item in mlss_data[mlss_start_idx:]]
		mlss_error = np.array([item[1] for item in mlss_data[mlss_start_idx:]])/ground_truth
		mlss_estimate = np.array([(item[0]-ground_truth)/ground_truth for item in mlss_data[mlss_start_idx:]])

		length = len(range(0, int(srs_trace[-1])+2))
		plt.fill_between(range(0, int(srs_trace[-1])+2), [-ci/ground_truth]*length, [ci/ground_truth]*length, \
			color='black', alpha=0.2, label='ground truth')

		plt.plot(srs_trace, srs_error, '-*', color='b', label='SRS CI')
		plt.plot(srs_trace, srs_error * -1, '-*', color='b')
		plt.plot(srs_trace, srs_estimate, ':', color='b', label='SRS estimate')

		plt.plot(mlss_trace, mlss_error, '-o', color='r', label='MLSS CI')
		plt.plot(mlss_trace, mlss_error * -1, '-o', color='r')
		plt.plot(mlss_trace, mlss_estimate, ':', color='r', label='MLSS estimate')

		plt.xlabel('second')
		plt.ylabel('estimation error (%)')
		plt.xlim([0, srs_trace[-1]])
		plt.legend(prop={'size': 12})
		plt.tight_layout()
		plt.show()