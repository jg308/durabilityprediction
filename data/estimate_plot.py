from matplotlib import pyplot as plt
import json
import argparse
import json
import numpy as np

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('srs_data', type=str, help='json file for srs data')
	parser.add_argument('mlss_data', type=str, help='json file for mlss data')
	parser.add_argument('seed', type=int, help='a random number')
	parser.add_argument('fig_type', type=str, help='figure type')

	args = parser.parse_args()

	with open(args.srs_data, 'r') as f:
		srs_data = json.load(f)
	with open(args.mlss_data, 'r') as f:
		mlss_data = json.load(f)
	idx = str(args.seed)
	fig_type = args.fig_type

	lower_ground_truth = srs_data['gt']-srs_data['ci']
	upper_ground_truth = srs_data['gt']+srs_data['ci']
	ground_truth = srs_data['gt']
	ci = srs_data['ci']

	print(ground_truth, ci)

	del srs_data['gt']
	del srs_data['ci']
	del mlss_data['gt']
	del mlss_data['ci']

	length = srs_data[idx][-1][2] - mlss_data[idx][0][2]

	if fig_type == 'est_path':
		plt.fill_between(range(mlss_data[idx][0][2], srs_data[idx][-1][2]), \
			[lower_ground_truth]*length, [upper_ground_truth]*length, \
			color='black', alpha=0.2, label='ground truth')

		plt.plot([item[2] for item in mlss_data[idx]], [item[0] for item in mlss_data[idx]], color='r', label='MLSS')
		plt.plot([item[2] for item in srs_data[idx]], [item[0] for item in srs_data[idx]], color='b', label='SRS')

		plt.legend()
		plt.show()

	if fig_type == 'avg_cost':
		srs_cost = [srs_data[key][-1][2] for key in srs_data]
		mlss_cost = [mlss_data[key][-1][2] for key in mlss_data]
		plt.bar(0, np.mean(srs_cost), yerr=np.std(srs_cost), color='b', label='SRS')
		plt.bar(1, np.mean(mlss_cost), yerr=np.std(mlss_cost), color='r', label='MLSS')
		plt.tick_params(
		    axis='x',          # changes apply to the x-axis
		    which='both',      # both major and minor ticks are affected
		    bottom=False,      # ticks along the bottom edge are off
		    top=False,         # ticks along the top edge are off
		    labelbottom=False) # labels along the bottom edge are off
		plt.legend()
		plt.show()

	if fig_type == 'ci_path':
		mlss_start_idx = 0
		mlss_ci = [item[1] / ground_truth for item in mlss_data[idx]]
		mlss_cost = [item[2] for item in mlss_data[idx]]
		for i, v in enumerate(mlss_ci):
			if v > 0:
				mlss_start_idx = i
				break
		srs_start_idx = 0
		srs_ci = [item[1] / ground_truth for item in srs_data[idx]]
		srs_cost = [item[2] for item in srs_data[idx]]
		for i,v in enumerate(srs_ci):
			if v > 0:
				srs_start_idx = i
				break

		plt.figure()

		plt.plot(mlss_cost[mlss_start_idx:], np.array(mlss_ci[mlss_start_idx:])*1, \
			color='r', label='MLSS CI')
		plt.plot(mlss_cost[mlss_start_idx:], np.array(mlss_ci[mlss_start_idx:])*-1, \
			color='r')
		plt.plot(mlss_cost, [(item[0]-ground_truth)/ground_truth for item in mlss_data[idx]], \
			':', color='r', label='MLSS estimate')

		plt.plot(srs_cost[srs_start_idx:], srs_ci[srs_start_idx:], \
			color='b', label='SRS CI')
		plt.plot(srs_cost[srs_start_idx:], np.array(srs_ci[srs_start_idx:])*-1, \
			color='b')
		plt.plot(srs_cost, [(item[0]-ground_truth)/ground_truth for item in srs_data[idx]], \
			':', color='b', label='SRS estimate')

		plt.fill_between(range(mlss_data[idx][0][2], srs_data[idx][-1][2]), \
			[-ci/ground_truth]*length, [ci/ground_truth]*length, \
			color='black', alpha=0.2, label='ground truth')

		plt.vlines(srs_cost[-1], -1, ci/ground_truth, color='b')
		plt.vlines(mlss_cost[-1], -1, ci/ground_truth, color='r')

		plt.xlabel('simulation steps')
		plt.ylabel('relative error')
		plt.ylim([-1, 1])
		plt.legend()
		plt.show()