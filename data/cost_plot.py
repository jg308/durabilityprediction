from matplotlib import pyplot as plt

def optimal_cost():
	# # 200 - 15
	# srs_cost = 549936.41
	# srs_cost_std = 28808.5075282
	# mlss_best_cost = 344254.37
	# mlss_best_cost_std = 25534.9174981
	# mlss_worst_cost = 412774.9
	# mlss_worst_cost_std = 31970.1434728
	# mlss_opt_cost = 387766.18
	# mlss_opt_cost_std = 62770.2555638

	# 1000 - 30
	srs_cost = 4487491.84 
	srs_cost_std = 108993.082047
	mlss_best_cost = 3036014.2 
	mlss_best_cost_std = 147568.515569
	mlss_worst_cost = 4144236.76 
	mlss_worst_cost_std = 127439.778191
	mlss_opt_cost = 3534818.06 
	mlss_opt_cost_std = 410172.519617


	plt.bar(0, srs_cost, yerr=srs_cost_std, color='b', label='SRS')
	plt.bar(1, mlss_best_cost, yerr=mlss_best_cost_std, label='MLSS-Best')
	plt.bar(2, mlss_opt_cost, yerr=mlss_opt_cost_std, color='r', label='MLSS-Opt')
	plt.bar(3, mlss_worst_cost, yerr=mlss_worst_cost_std, label='MLSS-Worst')

	plt.tick_params(
		    axis='x',          # changes apply to the x-axis
		    which='both',      # both major and minor ticks are affected
		    bottom=False,      # ticks along the bottom edge are off
		    top=False,         # ticks along the top edge are off
		    labelbottom=False) # labels along the bottom edge are off

	plt.ylabel('simulation steps')
	plt.legend()
	plt.show()

if __name__ == '__main__':
	optimal_cost()