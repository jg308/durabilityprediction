2020-05-07 21:30:15.398190: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer.so.6
2020-05-07 21:30:15.402369: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer_plugin.so.6
model:queue, T:500, V:22.0, CI:0.01
t-levels:None, v-levels:None, splits:3
trails:100, experiment type:search, error: 2
t-bounds:[100, 200, 300, 400, 500], v-bounds:[5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0, 21.0, 22.0], levels:2
[[100, 500], [200, 500], [300, 500], [400, 500], [500, 500]]
[[5.0, 22.0], [7.0, 22.0], [9.0, 22.0], [11.0, 22.0], [13.0, 22.0], [15.0, 22.0], [17.0, 22.0], [19.0, 22.0], [21.0, 22.0], [22.0, 22.0]]
Loading Queue Model...
SRS:0SRS:1SRS:2SRS:3SRS:4SRS:5SRS:6SRS:7SRS:8SRS:9SRS:10SRS:11SRS:12SRS:13SRS:14SRS:15SRS:16SRS:17SRS:18SRS:19SRS:20SRS:21SRS:22SRS:23SRS:24SRS:25SRS:26SRS:27SRS:28SRS:29SRS:30SRS:31SRS:32SRS:33SRS:34SRS:35SRS:36SRS:37SRS:38SRS:39SRS:40SRS:41SRS:42SRS:43SRS:44SRS:45SRS:46SRS:47SRS:48SRS:49SRS:50SRS:51SRS:52SRS:53SRS:54SRS:55SRS:56SRS:57SRS:58SRS:59SRS:60SRS:61SRS:62SRS:63SRS:64SRS:65SRS:66SRS:67SRS:68SRS:69SRS:70SRS:71SRS:72SRS:73SRS:74SRS:75SRS:76SRS:77SRS:78SRS:79SRS:80SRS:81SRS:82SRS:83SRS:84SRS:85SRS:86SRS:87SRS:88SRS:89SRS:90SRS:91SRS:92SRS:93SRS:94SRS:95SRS:96SRS:97SRS:98SRS:99=====SRS=====
estimate avg: 0.8821480374423912, std:0.004980433724865239
cost avg: 1936257.88, std:68273.292235878
avg time: 10.753434963226319
t-splits:[100, 400], v-splits:[5.0, 22.0]
*******NEW BEST********
estimate avg: 0.8797853425905298, std:0.004581276721427389
cost avg: 1808763.04, std:82921.03246003634
avg time: 10.185734632015228
t-splits:[100, 400], v-splits:[7.0, 22.0]
*******NEW BEST********
estimate avg: 0.8784024953948719, std:0.005388939661049265
cost avg: 1793064.47, std:79794.65236661602
avg time: 10.011416397094727
t-splits:[100, 400], v-splits:[9.0, 22.0]
*******NEW BEST********
estimate avg: 0.879493154099976, std:0.004355127943184578
cost avg: 1774107.55, std:88353.64526349492
avg time: 9.916540126800538
t-splits:[100, 400], v-splits:[11.0, 22.0]
t-splits:[100, 400], v-splits:[13.0, 22.0]
t-splits:[100, 400], v-splits:[15.0, 22.0]
t-splits:[100, 400], v-splits:[17.0, 22.0]
t-splits:[100, 400], v-splits:[19.0, 22.0]
t-splits:[100, 400], v-splits:[21.0, 22.0]
t-splits:[100, 400], v-splits:[22.0, 22.0]
t-splits:[200, 300], v-splits:[5.0, 22.0]
t-splits:[200, 300], v-splits:[7.0, 22.0]
*******NEW BEST********
estimate avg: 0.8790380112440954, std:0.0051485426138016205
cost avg: 1695704.97, std:78778.24763124081
avg time: 9.83024005651474
t-splits:[200, 300], v-splits:[9.0, 22.0]
*******NEW BEST********
estimate avg: 0.8798869148627325, std:0.005642154813750664
cost avg: 1651017.67, std:84212.7908103104
avg time: 9.487704892158508
t-splits:[200, 300], v-splits:[11.0, 22.0]
t-splits:[200, 300], v-splits:[13.0, 22.0]
t-splits:[200, 300], v-splits:[15.0, 22.0]
t-splits:[200, 300], v-splits:[17.0, 22.0]
t-splits:[200, 300], v-splits:[19.0, 22.0]
t-splits:[200, 300], v-splits:[21.0, 22.0]
t-splits:[200, 300], v-splits:[22.0, 22.0]
t-splits:[300, 200], v-splits:[5.0, 22.0]
t-splits:[300, 200], v-splits:[7.0, 22.0]
t-splits:[300, 200], v-splits:[9.0, 22.0]
*******NEW BEST********
estimate avg: 0.8791389478532414, std:0.005305058929507341
cost avg: 1566061.81, std:78178.27192381461
avg time: 9.183611476421357
t-splits:[300, 200], v-splits:[11.0, 22.0]
*******NEW BEST********
estimate avg: 0.8792510666870327, std:0.005518218000043785
cost avg: 1559412.33, std:82869.82923369095
avg time: 9.02124389410019
t-splits:[300, 200], v-splits:[13.0, 22.0]
t-splits:[300, 200], v-splits:[15.0, 22.0]
t-splits:[300, 200], v-splits:[17.0, 22.0]
t-splits:[300, 200], v-splits:[19.0, 22.0]
t-splits:[300, 200], v-splits:[21.0, 22.0]
t-splits:[300, 200], v-splits:[22.0, 22.0]
t-splits:[400, 100], v-splits:[5.0, 22.0]
t-splits:[400, 100], v-splits:[7.0, 22.0]
t-splits:[400, 100], v-splits:[9.0, 22.0]
t-splits:[400, 100], v-splits:[11.0, 22.0]
*******NEW BEST********
estimate avg: 0.8798844551917633, std:0.00495642806609695
cost avg: 1439753.25, std:73387.69971887319
avg time: 8.68016040802002
t-splits:[400, 100], v-splits:[13.0, 22.0]
t-splits:[400, 100], v-splits:[15.0, 22.0]
t-splits:[400, 100], v-splits:[17.0, 22.0]
t-splits:[400, 100], v-splits:[19.0, 22.0]
t-splits:[400, 100], v-splits:[21.0, 22.0]
t-splits:[400, 100], v-splits:[22.0, 22.0]
t-splits:[500, 0], v-splits:[5.0, 22.0]
t-splits:[500, 0], v-splits:[7.0, 22.0]
t-splits:[500, 0], v-splits:[9.0, 22.0]
t-splits:[500, 0], v-splits:[11.0, 22.0]
*******NEW BEST********
estimate avg: 0.8795946128986667, std:0.004898774085160442
cost avg: 1328627.2, std:65616.54301607178
avg time: 8.03738242149353
t-splits:[500, 0], v-splits:[13.0, 22.0]
*******NEW BEST********
estimate avg: 0.8790744318290813, std:0.005497868213012661
cost avg: 1280845.64, std:64297.98134366584
avg time: 7.625852789878845
t-splits:[500, 0], v-splits:[15.0, 22.0]
*******NEW BEST********
estimate avg: 0.8804944079650174, std:0.005108186260247075
cost avg: 1277834.92, std:64963.78882665019
avg time: 7.506165480613708
t-splits:[500, 0], v-splits:[17.0, 22.0]
t-splits:[500, 0], v-splits:[19.0, 22.0]
t-splits:[500, 0], v-splits:[21.0, 22.0]
t-splits:[500, 0], v-splits:[22.0, 22.0]
