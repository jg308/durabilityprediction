2020-05-07 17:58:58.002422: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer.so.6
2020-05-07 17:58:58.004802: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer_plugin.so.6
model:queue, T:500, V:10.0, CI:0.03
t-levels:None, v-levels:None, splits:3
trails:100, experiment type:search, error: 2
t-bounds:[100, 200, 300, 400, 500], v-bounds:[2.0, 4.0, 6.0, 8.0, 10.0], levels:2
[[100, 500], [200, 500], [300, 500], [400, 500], [500, 500]]
[[2.0, 10.0], [4.0, 10.0], [6.0, 10.0], [8.0, 10.0], [10.0, 10.0]]
Loading Queue Model...
SRS:0SRS:1SRS:2SRS:3SRS:4SRS:5SRS:6SRS:7SRS:8SRS:9SRS:10SRS:11SRS:12SRS:13SRS:14SRS:15SRS:16SRS:17SRS:18SRS:19SRS:20SRS:21SRS:22SRS:23SRS:24SRS:25SRS:26SRS:27SRS:28SRS:29SRS:30SRS:31SRS:32SRS:33SRS:34SRS:35SRS:36SRS:37SRS:38SRS:39SRS:40SRS:41SRS:42SRS:43SRS:44SRS:45SRS:46SRS:47SRS:48SRS:49SRS:50SRS:51SRS:52SRS:53SRS:54SRS:55SRS:56SRS:57SRS:58SRS:59SRS:60SRS:61SRS:62SRS:63SRS:64SRS:65SRS:66SRS:67SRS:68SRS:69SRS:70SRS:71SRS:72SRS:73SRS:74SRS:75SRS:76SRS:77SRS:78SRS:79SRS:80SRS:81SRS:82SRS:83SRS:84SRS:85SRS:86SRS:87SRS:88SRS:89SRS:90SRS:91SRS:92SRS:93SRS:94SRS:95SRS:96SRS:97SRS:98SRS:99=====SRS=====
estimate avg: 0.19544135033230375, std:0.015967681387591033
cost avg: 196980.93, std:15114.93711680932
avg time: 2.356535997390747
t-splits:[100, 400], v-splits:[2.0, 10.0]
*******NEW BEST********
estimate avg: 0.1913876125668806, std:0.014975935721027823
cost avg: 185815.29, std:20017.61293825765
avg time: 2.308790972232819
t-splits:[100, 400], v-splits:[4.0, 10.0]
*******NEW BEST********
estimate avg: 0.19056031727675943, std:0.017096193135699465
cost avg: 171261.57, std:20105.076389437072
avg time: 2.21650367975235
t-splits:[100, 400], v-splits:[6.0, 10.0]
*******NEW BEST********
estimate avg: 0.18660702566489476, std:0.01327531949953172
cost avg: 160027.48, std:18024.448014005866
avg time: 2.019007441997528
t-splits:[100, 400], v-splits:[8.0, 10.0]
t-splits:[100, 400], v-splits:[10.0, 10.0]
*******NEW BEST********
estimate avg: 0.19118027093482184, std:0.01559338637205056
cost avg: 161819.37, std:19742.84385424501
avg time: 1.985700147151947
t-splits:[200, 300], v-splits:[2.0, 10.0]
t-splits:[200, 300], v-splits:[4.0, 10.0]
t-splits:[200, 300], v-splits:[6.0, 10.0]
t-splits:[200, 300], v-splits:[8.0, 10.0]
t-splits:[200, 300], v-splits:[10.0, 10.0]
*******NEW BEST********
estimate avg: 0.1943119813308046, std:0.01601801575000066
cost avg: 153425.76, std:18461.33351040493
avg time: 1.6772227168083191
t-splits:[300, 200], v-splits:[2.0, 10.0]
t-splits:[300, 200], v-splits:[4.0, 10.0]
t-splits:[300, 200], v-splits:[6.0, 10.0]
t-splits:[300, 200], v-splits:[8.0, 10.0]
t-splits:[300, 200], v-splits:[10.0, 10.0]
*******NEW BEST********
estimate avg: 0.1911633695298602, std:0.015333380058566921
cost avg: 154747.63, std:16666.443568833154
avg time: 1.6586004710197448
t-splits:[400, 100], v-splits:[2.0, 10.0]
t-splits:[400, 100], v-splits:[4.0, 10.0]
t-splits:[400, 100], v-splits:[6.0, 10.0]
t-splits:[400, 100], v-splits:[8.0, 10.0]
t-splits:[400, 100], v-splits:[10.0, 10.0]
t-splits:[500, 0], v-splits:[2.0, 10.0]
t-splits:[500, 0], v-splits:[4.0, 10.0]
t-splits:[500, 0], v-splits:[6.0, 10.0]
t-splits:[500, 0], v-splits:[8.0, 10.0]
t-splits:[500, 0], v-splits:[10.0, 10.0]
