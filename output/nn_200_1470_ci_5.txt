2020-05-11 20:59:34.729855: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer.so.6
2020-05-11 20:59:34.732880: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer_plugin.so.6
2020-05-11 20:59:37.739001: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcuda.so.1
2020-05-11 20:59:37.741934: E tensorflow/stream_executor/cuda/cuda_driver.cc:351] failed call to cuInit: CUDA_ERROR_NO_DEVICE: no CUDA-capable device is detected
2020-05-11 20:59:37.741986: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:169] retrieving CUDA diagnostic information for host: linux49
2020-05-11 20:59:37.741994: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:176] hostname: linux49
2020-05-11 20:59:37.742092: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:200] libcuda reported version is: 418.56.0
2020-05-11 20:59:37.742125: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:204] kernel reported version is: 418.56.0
2020-05-11 20:59:37.742132: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:310] kernel version seems to match DSO: 418.56.0
2020-05-11 20:59:37.752498: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2020-05-11 20:59:37.780295: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2399875000 Hz
2020-05-11 20:59:37.780647: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x55979e78ad30 initialized for platform Host (this does not guarantee that XLA will be used). Devices:
2020-05-11 20:59:37.780677: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Host, Default Version
WARNING:tensorflow:From /home/home1/jygao/.local/lib/python3.7/site-packages/tensorflow_core/python/ops/linalg/linear_operator_diag.py:166: calling LinearOperator.__init__ (from tensorflow.python.ops.linalg.linear_operator) with graph_parents is deprecated and will be removed in a future version.
Instructions for updating:
Do not pass `graph_parents`.  They will  no longer be used.
model:nn, T:200, V:1470.0, CI:0.05
t-levels:None, v-levels:None, splits:3
trails:10, experiment type:search, error: 2
t-bounds:[50, 100, 150, 200], v-bounds:[1400.0, 1420.0, 1440.0, 1450.0, 1470.0], levels:2
[[50, 200], [100, 200], [150, 200], [200, 200]]
[[1400.0, 1470.0], [1420.0, 1470.0], [1440.0, 1470.0], [1450.0, 1470.0], [1470.0, 1470.0]]
Loading RNN-MDN Model
Num CPUs: 1
Num GPUs: 0
Model Loaded!
SRS:0SRS:1SRS:2SRS:3SRS:4SRS:5SRS:6SRS:7SRS:8SRS:9=====SRS=====
estimate avg: 0.9252480158730159, std:0.033000190932302255
cost avg: 22180.2, std:9240.585401369332
avg time: 545.3354298353195
t-splits:[50, 150], v-splits:[1400.0, 1470.0]
*******NEW BEST********
estimate avg: 0.9344444444444443, std:0.01786782749131129
cost avg: 20553.8, std:5642.649834962293
avg time: 519.9670676708222
t-splits:[50, 150], v-splits:[1420.0, 1470.0]
t-splits:[50, 150], v-splits:[1440.0, 1470.0]
t-splits:[50, 150], v-splits:[1450.0, 1470.0]
t-splits:[50, 150], v-splits:[1470.0, 1470.0]
t-splits:[100, 100], v-splits:[1400.0, 1470.0]
t-splits:[100, 100], v-splits:[1420.0, 1470.0]
t-splits:[100, 100], v-splits:[1440.0, 1470.0]
t-splits:[100, 100], v-splits:[1450.0, 1470.0]
t-splits:[100, 100], v-splits:[1470.0, 1470.0]
t-splits:[150, 50], v-splits:[1400.0, 1470.0]
*******NEW BEST********
estimate avg: 0.9301626984126985, std:0.03976396586395185
cost avg: 18010.4, std:9552.472100979934
avg time: 397.89426987171174
t-splits:[150, 50], v-splits:[1420.0, 1470.0]
t-splits:[150, 50], v-splits:[1440.0, 1470.0]
t-splits:[150, 50], v-splits:[1450.0, 1470.0]
t-splits:[150, 50], v-splits:[1470.0, 1470.0]
t-splits:[200, 0], v-splits:[1400.0, 1470.0]
*******NEW BEST********
estimate avg: 0.9191031746031747, std:0.0326127962958919
cost avg: 16051.8, std:6815.3227040251
avg time: 394.1015935182571
t-splits:[200, 0], v-splits:[1420.0, 1470.0]
t-splits:[200, 0], v-splits:[1440.0, 1470.0]
t-splits:[200, 0], v-splits:[1450.0, 1470.0]
t-splits:[200, 0], v-splits:[1470.0, 1470.0]
