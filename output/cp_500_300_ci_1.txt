2020-05-07 21:31:42.007807: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer.so.6
2020-05-07 21:31:42.010435: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer_plugin.so.6
model:cp, T:500, V:300.0, CI:0.01
t-levels:None, v-levels:None, splits:3
trails:100, experiment type:search, error: 2
t-bounds:[100, 200, 300, 400, 500], v-bounds:[50.0, 70.0, 90.0, 100.0, 120.0, 150.0, 180.0, 200.0, 220.0, 250.0, 280.0, 300.0], levels:2
[[100, 500], [200, 500], [300, 500], [400, 500], [500, 500]]
[[50.0, 300.0], [70.0, 300.0], [90.0, 300.0], [100.0, 300.0], [120.0, 300.0], [150.0, 300.0], [180.0, 300.0], [200.0, 300.0], [220.0, 300.0], [250.0, 300.0], [280.0, 300.0], [300.0, 300.0]]
Loading Compound-Poisson model
SRS:0SRS:1SRS:2SRS:3SRS:4SRS:5SRS:6SRS:7SRS:8SRS:9SRS:10SRS:11SRS:12SRS:13SRS:14SRS:15SRS:16SRS:17SRS:18SRS:19SRS:20SRS:21SRS:22SRS:23SRS:24SRS:25SRS:26SRS:27SRS:28SRS:29SRS:30SRS:31SRS:32SRS:33SRS:34SRS:35SRS:36SRS:37SRS:38SRS:39SRS:40SRS:41SRS:42SRS:43SRS:44SRS:45SRS:46SRS:47SRS:48SRS:49SRS:50SRS:51SRS:52SRS:53SRS:54SRS:55SRS:56SRS:57SRS:58SRS:59SRS:60SRS:61SRS:62SRS:63SRS:64SRS:65SRS:66SRS:67SRS:68SRS:69SRS:70SRS:71SRS:72SRS:73SRS:74SRS:75SRS:76SRS:77SRS:78SRS:79SRS:80SRS:81SRS:82SRS:83SRS:84SRS:85SRS:86SRS:87SRS:88SRS:89SRS:90SRS:91SRS:92SRS:93SRS:94SRS:95SRS:96SRS:97SRS:98SRS:99=====SRS=====
estimate avg: 0.84479517498086, std:0.005175944516974863
cost avg: 2458757.1, std:65223.91012864837
avg time: 16.07639973402023
t-splits:[100, 400], v-splits:[50.0, 300.0]
*******NEW BEST********
estimate avg: 0.8453610104428898, std:0.004656847430526748
cost avg: 2435444.2, std:89810.70584468202
avg time: 15.935023603439332
t-splits:[100, 400], v-splits:[70.0, 300.0]
t-splits:[100, 400], v-splits:[90.0, 300.0]
t-splits:[100, 400], v-splits:[100.0, 300.0]
t-splits:[100, 400], v-splits:[120.0, 300.0]
t-splits:[100, 400], v-splits:[150.0, 300.0]
t-splits:[100, 400], v-splits:[180.0, 300.0]
t-splits:[100, 400], v-splits:[200.0, 300.0]
t-splits:[100, 400], v-splits:[220.0, 300.0]
t-splits:[100, 400], v-splits:[250.0, 300.0]
t-splits:[100, 400], v-splits:[280.0, 300.0]
t-splits:[100, 400], v-splits:[300.0, 300.0]
t-splits:[200, 300], v-splits:[50.0, 300.0]
t-splits:[200, 300], v-splits:[70.0, 300.0]
*******NEW BEST********
estimate avg: 0.8448248819528809, std:0.004802678242051888
cost avg: 2362914.27, std:93452.72539566248
avg time: 15.87220847606659
t-splits:[200, 300], v-splits:[90.0, 300.0]
t-splits:[200, 300], v-splits:[100.0, 300.0]
t-splits:[200, 300], v-splits:[120.0, 300.0]
t-splits:[200, 300], v-splits:[150.0, 300.0]
t-splits:[200, 300], v-splits:[180.0, 300.0]
t-splits:[200, 300], v-splits:[200.0, 300.0]
t-splits:[200, 300], v-splits:[220.0, 300.0]
t-splits:[200, 300], v-splits:[250.0, 300.0]
t-splits:[200, 300], v-splits:[280.0, 300.0]
t-splits:[200, 300], v-splits:[300.0, 300.0]
t-splits:[300, 200], v-splits:[50.0, 300.0]
t-splits:[300, 200], v-splits:[70.0, 300.0]
t-splits:[300, 200], v-splits:[90.0, 300.0]
*******NEW BEST********
estimate avg: 0.8445727782400784, std:0.005150979263593111
cost avg: 2294505.98, std:95393.56643851618
avg time: 15.309821350574493
t-splits:[300, 200], v-splits:[100.0, 300.0]
*******NEW BEST********
estimate avg: 0.8450026950042978, std:0.005139201746480002
cost avg: 2278737.37, std:81114.2670937308
avg time: 14.961952483654022
t-splits:[300, 200], v-splits:[120.0, 300.0]
*******NEW BEST********
estimate avg: 0.8457292733523064, std:0.004757189336001759
cost avg: 2267324.62, std:83050.03377817254
avg time: 14.776110327243805
t-splits:[300, 200], v-splits:[150.0, 300.0]
t-splits:[300, 200], v-splits:[180.0, 300.0]
t-splits:[300, 200], v-splits:[200.0, 300.0]
t-splits:[300, 200], v-splits:[220.0, 300.0]
t-splits:[300, 200], v-splits:[250.0, 300.0]
t-splits:[300, 200], v-splits:[280.0, 300.0]
t-splits:[300, 200], v-splits:[300.0, 300.0]
t-splits:[400, 100], v-splits:[50.0, 300.0]
t-splits:[400, 100], v-splits:[70.0, 300.0]
t-splits:[400, 100], v-splits:[90.0, 300.0]
t-splits:[400, 100], v-splits:[100.0, 300.0]
t-splits:[400, 100], v-splits:[120.0, 300.0]
t-splits:[400, 100], v-splits:[150.0, 300.0]
*******NEW BEST********
estimate avg: 0.8451235403723456, std:0.00436219049503314
cost avg: 2117149.84, std:68319.40567535991
avg time: 14.423671381473541
t-splits:[400, 100], v-splits:[180.0, 300.0]
*******NEW BEST********
estimate avg: 0.8463753510243979, std:0.005005685160855811
cost avg: 2115741.51, std:70654.91519427292
avg time: 14.080705049037933
t-splits:[400, 100], v-splits:[200.0, 300.0]
t-splits:[400, 100], v-splits:[220.0, 300.0]
t-splits:[400, 100], v-splits:[250.0, 300.0]
t-splits:[400, 100], v-splits:[280.0, 300.0]
t-splits:[400, 100], v-splits:[300.0, 300.0]
t-splits:[500, 0], v-splits:[50.0, 300.0]
t-splits:[500, 0], v-splits:[70.0, 300.0]
t-splits:[500, 0], v-splits:[90.0, 300.0]
t-splits:[500, 0], v-splits:[100.0, 300.0]
t-splits:[500, 0], v-splits:[120.0, 300.0]
t-splits:[500, 0], v-splits:[150.0, 300.0]
t-splits:[500, 0], v-splits:[180.0, 300.0]
*******NEW BEST********
estimate avg: 0.8449076538281427, std:0.004960759427862156
cost avg: 1913126.45, std:72575.43383216872
avg time: 13.702943823337556
t-splits:[500, 0], v-splits:[200.0, 300.0]
*******NEW BEST********
estimate avg: 0.8456982825264641, std:0.0050350946564452275
cost avg: 1876012.19, std:68331.030472794
avg time: 13.360605812072754
t-splits:[500, 0], v-splits:[220.0, 300.0]
t-splits:[500, 0], v-splits:[250.0, 300.0]
t-splits:[500, 0], v-splits:[280.0, 300.0]
t-splits:[500, 0], v-splits:[300.0, 300.0]
