2020-05-11 22:15:17.316706: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer.so.6
2020-05-11 22:15:17.321171: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libnvinfer_plugin.so.6
2020-05-11 22:15:36.735492: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcuda.so.1
2020-05-11 22:15:36.740598: E tensorflow/stream_executor/cuda/cuda_driver.cc:351] failed call to cuInit: CUDA_ERROR_NO_DEVICE: no CUDA-capable device is detected
2020-05-11 22:15:36.740648: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:169] retrieving CUDA diagnostic information for host: linux41
2020-05-11 22:15:36.740657: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:176] hostname: linux41
2020-05-11 22:15:36.740746: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:200] libcuda reported version is: 418.56.0
2020-05-11 22:15:36.740779: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:204] kernel reported version is: 418.56.0
2020-05-11 22:15:36.740787: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:310] kernel version seems to match DSO: 418.56.0
2020-05-11 22:15:36.758075: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2020-05-11 22:15:36.787151: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2399975000 Hz
2020-05-11 22:15:36.787488: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x55d52f197210 initialized for platform Host (this does not guarantee that XLA will be used). Devices:
2020-05-11 22:15:36.787520: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Host, Default Version
WARNING:tensorflow:From /home/home1/jygao/.local/lib/python3.7/site-packages/tensorflow_core/python/ops/linalg/linear_operator_diag.py:166: calling LinearOperator.__init__ (from tensorflow.python.ops.linalg.linear_operator) with graph_parents is deprecated and will be removed in a future version.
Instructions for updating:
Do not pass `graph_parents`.  They will  no longer be used.
model:nn, T:500, V:1290.0, CI:0.05
t-levels:None, v-levels:None, splits:3
trails:10, experiment type:search, error: 2
t-bounds:[100, 200, 300, 400, 500], v-bounds:[1270.0, 1280.0, 1290.0], levels:2
[[100, 500], [200, 500], [300, 500], [400, 500], [500, 500]]
[[1270.0, 1290.0], [1280.0, 1290.0], [1290.0, 1290.0]]
Loading RNN-MDN Model
google_stock.csv model/google_rnn_mdn_2_5_16_30.h5
Num CPUs: 1
Num GPUs: 0
Model Loaded!
SRS:0SRS:1SRS:2SRS:3SRS:4SRS:5SRS:6SRS:7SRS:8SRS:9=====SRS=====
estimate avg: 0.09568253968253969, std:0.027839188353169803
cost avg: 14398.8, std:4486.415179182596
avg time: 297.92173359394076
t-splits:[100, 400], v-splits:[1270.0, 1290.0]
*******NEW BEST********
estimate avg: 0.08871825396825397, std:0.03484884417975946
cost avg: 15199.4, std:9206.610094926362
avg time: 334.9768620967865
t-splits:[100, 400], v-splits:[1280.0, 1290.0]
t-splits:[100, 400], v-splits:[1290.0, 1290.0]
t-splits:[200, 300], v-splits:[1270.0, 1290.0]
t-splits:[200, 300], v-splits:[1280.0, 1290.0]
*******NEW BEST********
estimate avg: 0.07197293447293447, std:0.0361679375808155
cost avg: 12230.8, std:9279.675617175419
avg time: 281.37901082038877
t-splits:[200, 300], v-splits:[1290.0, 1290.0]
t-splits:[300, 200], v-splits:[1270.0, 1290.0]
t-splits:[300, 200], v-splits:[1280.0, 1290.0]
t-splits:[300, 200], v-splits:[1290.0, 1290.0]
t-splits:[400, 100], v-splits:[1270.0, 1290.0]
t-splits:[400, 100], v-splits:[1280.0, 1290.0]
t-splits:[400, 100], v-splits:[1290.0, 1290.0]
t-splits:[500, 0], v-splits:[1270.0, 1290.0]
t-splits:[500, 0], v-splits:[1280.0, 1290.0]
t-splits:[500, 0], v-splits:[1290.0, 1290.0]
*******NEW BEST********
estimate avg: 0.06411247086247086, std:0.03278438040430774
cost avg: 8545.8, std:4777.766733527287
avg time: 138.03420379161835
