import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import scipy.stats as ss
import seaborn as sns
import random

NOISE_SIGMA = 5
MAX_STEP = 100
POPULATION = 100
TRAILS = 5000

def ind_white_noises(size, sigma):
	return np.random.normal(0, sigma, size).reshape(POPULATION,1)

def average(l):
	return sum(l) / len(l)

def deviation(l):
	avg = average(l)
	return sum([(x - avg)**2 for x in l]) / len(l)

def cluster_test(target, topk_boundary):
	rank_series = []
	FHT = []

	# use VAR(1) model
	X = np.array(list(range(POPULATION))).reshape(POPULATION, 1)
	rank_X = ss.rankdata(np.transpose(X))
	initial_rank = rank_X[target]
	print('target:{}, rank:{}, boundary:{}'.format(target, rank_X[target], topk_boundary))
	np.random.seed(25)
	coff = np.identity(POPULATION) * np.random.rand(POPULATION, POPULATION)
	constant = []
	for i in range(POPULATION):
		constant.append(X[i,0] * (1 - coff[i,i]))
	constant = np.array(constant).reshape(POPULATION, 1)

	initial_state = X

	for i in range(TRAILS):
		step = 0
		# recover
		X = initial_state
		target_rank = []
		target_rank.append(initial_rank)
		while step < MAX_STEP:
			errors = ind_white_noises(POPULATION, NOISE_SIGMA)
			X = np.matmul(coff, X) + constant + errors
			rank_X = ss.rankdata(np.transpose(X))
			target_rank.append(rank_X[target])
			if rank_X[target] <= topk_boundary:
				step+=1
			else:
				break
		rank_series.append(target_rank)
		FHT.append(step)

	# plt.subplot(1,2,1)
	# sns.distplot(FHT, hist=True, kde=False, label='FULL')
	# plt.subplot(1,2,2)
	cluster1 = defaultdict(list)
	cluster1_idx = defaultdict(list)
	for idx,series in enumerate(rank_series):
		cluster1[str(int(series[1] <= initial_rank))].append(FHT[idx])
		cluster1_idx[str(int(series[1] <= initial_rank))].append(idx)
	print(cluster1.keys())
	for c in cluster1:
		print('{}:{} - mean:{}/variance:{}'.format(c, len(cluster1[c]), \
			average(cluster1[c]), deviation(cluster1[c])))
	labels, data = [*zip(*cluster1.items())]
	plt.boxplot(data)
	plt.xticks(range(1, len(labels) + 1), labels)
	plt.show()
	# for idx,c in enumerate(cluster.keys()):
	# 	print('{}:{}'.format(c, len(cluster[c])))
	# 	plt.subplot(len(cluster),1,idx+1)
	# 	plt.title('{}'.format(c))
	# 	sns.distplot(cluster[c], hist=True, kde=False)
	# 	plt.xlim(0,100)
	# plt.tight_layout()
	cluster2 = defaultdict(list)
	cluster2_idx = defaultdict(list)
	for c in cluster1_idx:
		for idx in cluster1_idx[c]:
			if len(rank_series[idx]) > 2:
				cluster2[c + str(int(rank_series[idx][2] <= initial_rank))].append(FHT[idx])
				cluster2_idx[c + str(int(rank_series[idx][2] <= initial_rank))].append(idx)
	print(cluster2.keys())
	for c in cluster2:
		print('{}:{} - mean:{}/variance:{}'.format(c, len(cluster2[c]), \
			average(cluster2[c]), deviation(cluster2[c])))
	labels, data = [*zip(*cluster2.items())]
	plt.boxplot(data)
	plt.xticks(range(1, len(labels) + 1), labels)
	plt.show()

	cluster3 = defaultdict(list)
	cluster3_idx = defaultdict(list)
	for c in cluster2_idx:
		for idx in cluster2_idx[c]:
			if len(rank_series[idx]) > 3:
				cluster3[c + str(int(rank_series[idx][3] <= initial_rank))].append(FHT[idx])
				cluster3_idx[c + str(int(rank_series[idx][3] <= initial_rank))].append(idx)
	print(cluster3.keys())
	for c in cluster3:
		print('{}:{} - mean:{}/variance:{}'.format(c, len(cluster3[c]), \
			average(cluster3[c]), deviation(cluster3[c])))
	labels, data = [*zip(*cluster3.items())]
	plt.boxplot(data)
	plt.xticks(range(1, len(labels) + 1), labels)
	plt.show()

	cluster4 = defaultdict(list)
	cluster4_idx = defaultdict(list)
	for c in cluster3_idx:
		for idx in cluster3_idx[c]:
			if len(rank_series[idx]) > 4:
				cluster4[c + str(int(rank_series[idx][4] <= initial_rank))].append(FHT[idx])
				cluster4_idx[c + str(int(rank_series[idx][4] <= initial_rank))].append(idx)
	print(cluster4.keys())
	for c in cluster4:
		print('{}:{} - mean:{}/variance:{}'.format(c, len(cluster4[c]), \
			average(cluster4[c]), deviation(cluster4[c])))
	labels, data = [*zip(*cluster4.items())]
	plt.boxplot(data)
	plt.xticks(range(1, len(labels) + 1), labels)
	plt.show()

if __name__ == '__main__':
	cluster_test(5, 20)
