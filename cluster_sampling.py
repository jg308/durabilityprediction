import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import random
import copy
from collections import defaultdict

from multilevel_spliting import *

# batch size
BS = 100
# expand size
ES = 10

def SRS_v2(T, level, batch_size):
	total_cost = 0
	estimate = 0
	samples = []

	history = []
	for i in range(batch_size):
		s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, level)
		t = len(s2)
		total_cost += t
		if s2[-1] >= level:
			samples.append(1)
		else:
			samples.append(0)
		if i > 0 and i % 10 == 0:
			estimate = np.mean(samples)
			history.append((estimate, total_cost))

	return history

def SRS(T, level, ci_threshold, verbose=False):
	# print('=====Start of SRS=====')
	total_cost = 0
	estimate = 0
	ci = 1
	samples = []

	history = []
	while ci > ci_threshold:
		for i in range(10):
			s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, level)
			t = len(s2)
			total_cost += t
			if t >= T and s2[-1] < level:
				samples.append(0)
			else:
				samples.append(1)
		estimate = np.mean(samples)
		ci = 1.96*np.sqrt(np.var(samples) / len(samples))
		if verbose:
			print(len(samples), total_cost, estimate, ci)
		history.append((estimate, ci, total_cost))

	return estimate, ci, total_cost, history

def CLS(split_level, T, level, ci_threshold, verbose=False):
	print('=====Start of CLS=====')
	total_cost = 0
	cluster_state = defaultdict(list)

	for i in range(BS):
		s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_level)
		t = len(s2)
		total_cost += t
		if t >= T and s2[-1] < split_level:
			cluster_state['FAIL'].append(0)
		else:
			cluster_state['-'.join([str(split_level), str(t)])].append(\
				(s1[-1], s2[-1], t))
	print('split level:{}, total cluster:{}, initial cost:{}'.format(\
		split_level, len(cluster_state), total_cost))

	# weight cluster sampling pps (approximated by empircal probability)
	weighted_states = np.random.choice(list(cluster_state.keys()), \
		2000, p=[len(cluster_state[k]) / BS for k in cluster_state])

	estimate = 0
	ci = 1
	start_idx = 0
	samples = []
	while ci > ci_threshold:
		if start_idx >= 2000:
			print('out of samples...')
			break
		for i in range(10):
			state = weighted_states[start_idx]
			start_idx += 1
			if state == 'FAIL':
				samples.append(0)
			else:
				valid_cnt = 0
				for trail in range(ES):
					rdm_idx = np.random.choice(range(len(cluster_state[state])),1)[0]
					q1,q2,t = cluster_state[state][rdm_idx]
					s1,s2 = tandem_queue(q1,q2,T-t,0.5,2,2,level)
					total_cost += len(s2)
					if len(s2) > 0 and s2[-1] >= level:
						valid_cnt += 1
				samples.append(valid_cnt / ES)
		estimate = np.mean(samples)
		ci = 1.96 * np.sqrt(np.var(samples) / len(samples))
		print(total_cost, estimate, ci)

	return estimate, ci, total_cost


def cluster_sampling_single_level(split_level, T, level, ground_truth, verbose=False):
	total_cost = 0
	# key : level + time, value : previous state
	cluster_state = defaultdict(list)

	# initial state: generate cluster state
	for i in range(BS):
		sequence1, sequence2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_level)
		t = len(sequence2)
		total_cost += t
		# fail state:
		if len(sequence2) == T and sequence2[-1] < split_level:
			cluster_state['FAIL'].append(0)
		# other cluster state:
		else:
			cluster_state['-'.join([str(split_level), str(t)])].append(\
				(sequence1[-1], sequence2[-1], t))

	print('total cluster:{}, initial cost:{}'.format(\
		len(cluster_state), total_cost))

	# weight cluster sampling pps (approximated by empircal probability)
	weighted_states = np.random.choice(list(cluster_state.keys()), \
		1000, p=[len(cluster_state[k]) / BS for k in cluster_state])

	# weighted cluster sampling estimation
	sample_label = []
	for size in range(0,1000,10):
		samples = weighted_states[size:size+10]
		# expand the cluster state to estimated the succuess process
		for state in samples:
			if state == 'FAIL':
				sample_label.append(0)
			else:
				#print('====={}====='.format(state))
				valid_cnt = 0
				for i in range(ES):
					idx = np.random.choice(range(len(cluster_state[state])),1)[0]
					q1,q2,t = cluster_state[state][idx]
					s1,s2 = tandem_queue(q1,q2,T-t,0.5,2,2,level)
					#print(q1,q2,t)
					total_cost += len(s2)
					if len(s2) > 0 and s2[-1] >= level:
						valid_cnt += 1
				sample_label.append(valid_cnt / ES)
		#print(sample_label)
		print('sample size:{} - estimation:{} - ci:{} - cost:{}'.format(\
			len(sample_label),np.mean(sample_label),\
			1.96*np.sqrt(np.var(sample_label) / len(sample_label)), total_cost))

	return cluster_state

def cluster_sampling_multi_levels(l1, l2, T, level, ci_threshold, verbose=False):
	total_cost = 0
	level_state = []
	cluster_state = defaultdict(list)
	l1_fails = 0
	l2_fails = 0

	# first level
	for i in range(BS):
		s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, l1)
		t = len(s2)
		total_cost += t
		if s2[-1] >= l1:
			level_state.append((s1[-1], s2[-1], t))
		else:
			l1_fails += 1

	print('first level failure rate:{}'.format(l1_fails / BS))

	# second level
	for i in range(BS):
		if i < len(level_state):
			idx = i
		else:
			idx = np.random.choice(range(len(level_state)), 1)[0]
		q1, q2, t = level_state[idx]
		s1, s2 = tandem_queue(q1, q2, T-t, 0.5, 2, 2, l2)
		total_cost += len(s2)
		if len(s2) > 0 and s2[-1] >= l2:
			cluster_state[t+len(s2)].append((s1[-1],s2[-1],t+len(s2)))
		else:
			l2_fails += 1

	print('second level failure rate:{}'.format(l2_fails / BS))

	cluster_state['Fail-1'].extend([0]*l1_fails)
	cluster_state['Fail-2'].extend([0]*l2_fails)
	total_states = np.sum([len(cluster_state[k]) for k in cluster_state])
	

	print('initial clustering cost:{}, clusters:{}'.format(total_cost, len(cluster_state)))

	# weighted cluster sampling
	weighted_states = np.random.choice(list(cluster_state.keys()), \
		2000, p=[len(cluster_state[k]) / total_states for k in cluster_state])

	# weighted cluster sampling estimation
	estimate = 0
	ci = 1
	start_idx = 0
	samples = []
	
	while ci > ci_threshold:
		if start_idx >= 2000:
			print('out of samples...')
			break
		for i in range(50):
			state = weighted_states[start_idx]
			start_idx += 1
			if state == 'Fail-1' or state == 'Fail-2':
				samples.append(0)
			else:
				valid_cnt = 0
				for trail in range(ES):
					state = int(state)
					rdm_idx = np.random.choice(range(len(cluster_state[state])),1)[0]
					q1,q2,t = cluster_state[state][rdm_idx]
					s1,s2 = tandem_queue(q1,q2,T-t,0.5,2,2,level)
					total_cost += len(s2)
					if len(s2) > 0 and s2[-1] >= level:
						valid_cnt += 1
				samples.append(valid_cnt / ES)
		estimate = np.mean(samples)
		ci = 1.96 * np.sqrt(np.var(samples) / len(samples))
		print(total_cost, estimate, ci)

	return estimate, ci, total_cost


def stratified_sampling_single_level(split_level, T, level, verbose=False):
	print('===start of SCS===')
	total_cost = 0
	level_state = []
	l1_fails = 0

	# first level
	for i in range(BS):
		s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_level)
		t = len(s2)
		total_cost += t
		if s2[-1] >= split_level:
			level_state.append((s1[-1], s2[-1], t))
		else:
			l1_fails += 1

	print('first level failure rate:{}'.format(l1_fails / BS))

	# stratified sampling
	sample_labels = []
	rdm_idx = np.random.choice(range(len(level_state)), BS)
	for idx in rdm_idx:
		q1, q2, t = level_state[idx]
		s1, s2 = tandem_queue(q1, q2, T-t, 0.5, 2, 2, level)
		total_cost += len(s2)
		if len(s2) > 0 and s2[-1] >= level:
			sample_labels.append(1)
		else:
			sample_labels.append(0)

	est_var = np.var(sample_labels) / len(sample_labels)

	# stratified estimator
	w1 = l1_fails / BS
	w2 = len(level_state) / BS
	assert l1_fails + len(level_state) == BS

	estimate = w1 * 0 + w2 * np.mean(sample_labels)
	ci = 1.96*np.sqrt(w1*0 + w2 * est_var)

	print(estimate, ci, total_cost)


def cluster_sampling_prob_distribution(cluster_state, T, level, ground_truth, verbose=False):

	cluster_sizes = {}
	for state in cluster_state:
		if state == 'FAIL':
			cluster_sizes[T+1] = len(cluster_state['FAIL']) / BS
		else:
			cluster_sizes[int(state.split('-')[-1])] = len(cluster_state[state]) / BS

	print('total cluster:{}'.format(len(cluster_state)))

	state_prob = {}	
	for state in cluster_state.keys():
		if state == 'FAIL':
			continue
		valid_cnt = 0
		for i in range(ES):
			idx = np.random.choice(range(len(cluster_state[state])),1)[0]
			q1,q2,t = cluster_state[state][idx]
			s1,s2 = tandem_queue(q1,q2,T-t,0.5,2,2,level)
			if len(s2) > 0 and s2[-1] >= level:
				valid_cnt += 1
		state_prob[int(state.split('-')[-1])] = valid_cnt / ES

	plt.bar(sorted(list(state_prob.keys())), [state_prob[k] for k in sorted(list(state_prob.keys()))], label='cluster success prob')
	plt.bar(sorted(list(cluster_sizes.keys())), [cluster_sizes[k] for k in sorted(list(cluster_sizes.keys()))], label='cluster size dist')
	plt.legend()
	plt.show()


if __name__ == '__main__':
	timeframe = 200
	boundary = 10
	ci_threshold = 0.01
	# SRS(timeframe, boundary, ci_threshold)
	# cluster_sampling_multi_levels(12, 16, timeframe, boundary, ci_threshold)
	# CLS(6, timeframe, boundary, ci_threshold)
	# ground truth
	ground_truth = simple_random_sampling(0.01, 50000, timeframe, boundary, plot_id=None)
	# stratified_sampling_single_level(8, timeframe, boundary)
	# simple_random_sampling_with_cost(1000, timeframe, boundary, ground_truth, verbose=True)
	# states = cluster_sampling_single_level(6, timeframe, boundary, ground_truth)

	# cluster_sampling_prob_distribution(states, timeframe, boundary, ground_truth)