import random
import numpy as np
from scipy.stats import truncnorm
from collections import defaultdict
import matplotlib.pyplot as plt

MAX_LEN = 100
THRESHOLD = 3

def path_simulation(phi, sigma, trails, initial, boundary=THRESHOLD, limit=MAX_LEN):
	FHT = 0
	for i in range(trails):
		length = 0
		x = initial
		error = np.random.normal(0, sigma, limit+1)
		while x < boundary and length < limit:
			length+=1
			x = phi * x + error[length]
		FHT += length
	#print('path simulation : {}'.format(FHT / trails))

	return FHT/trails

def cdf_FHT_estimation(threshold, value_dict, limit=MAX_LEN):
	threshold_dict = {}
	for t in value_dict.keys():
		threshold_dict[t] = len([i for i in value_dict[t] if i >= threshold]) / len(value_dict[t])

	#print(threshold_dict)

	FHT = 0
	for t in sorted(value_dict.keys()):
		failure = 1
		for prev_t in range(t):
			if prev_t not in value_dict.keys():
				continue
			failure *= (1 - threshold_dict[prev_t])
		#print(failure)
		FHT += failure * threshold_dict[t] * t

	#print('cdf simulation : {}'.format(FHT))
	return FHT



def cdf_simulation(phi, sigma, trails, initial, boundary=THRESHOLD, limit=MAX_LEN):
	value_dict = defaultdict(list)
	for i in range(trails):
		length = 0
		x = initial
		error = np.random.normal(0, sigma, limit+1)
		while x < boundary and length < limit:
			length+=1
			x = phi * x + error[length]
			value_dict[length].append(x)

	return cdf_FHT_estimation(boundary, value_dict)

if __name__ == '__main__':
	path_values = []
	cdf_values = []
	for i in range(1000):
		if i % 100 == 0:
			print(i)
		path_values.append(path_simulation(0.3, 2, 100, 0))
		cdf_values.append(cdf_simulation(0.3, 2, 1000, 0))

	print('path estimates: {} / {}'.format(np.mean(path_values), np.std(path_values)))
	print('cdf estimates: {} / {}'.format(np.mean(cdf_values), np.std(cdf_values)))
			