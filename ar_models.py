import random
import numpy as np
import copy
from collections import defaultdict
from matplotlib import pyplot as plt

class AR_model:
	def __init__(self, params, constant, error, Z):
		self.params = params
		self.c = constant
		self.mu, self.sigma = error
		self.Z = Z
		self.hits = {}

	def noise(self, T):
		return np.random.normal(self.mu, self.sigma, T)

	def durability_function(self, sequence, bound):
		# counter = 0
		# for v in sequence:
		# 	if v >= bound:
		# 		counter += 1
		# return counter
		return sequence[-1]

	def initialize(self, t_boundaries, v_boundaries):
		
		self.hits.clear()
		
		for i in range(len(v_boundaries)):
			self.hits[i] = []

	def simulate(self, T, V, initital_state, bound, plot=False):
		sequence = []
		white_noise = self.noise(T)
		state = list(initital_state)
		for t in range(T):
			#print(state, white_noise[t])
			x = self.c
			for item in zip(self.params, state):
				x += item[0] * item[1]
			x += white_noise[t]
			sequence.append(x)
			state = state[1:]
			state.append(x)
			# if x >= V:
			if self.durability_function(sequence, bound) >= V:
				break

		if plot:
			print(sequence)
			plt.plot(sequence, color='black', label='AR')
			plt.hlines(bound, 0, T, color='r')
			plt.legend()
			plt.show()

		return sequence

	def SRS(self, T, V, initial_state, bound, ci_threshold, verbose=False):
		total_cost = 0
		estimate = 0
		ci = 1
		samples = []

		history = []

		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(10):
				s = self.simulate(T, V, initial_state, bound)
				t = len(s)
				total_cost += t
				# if t > 0 and s[-1] >= V:
				# if self.durability_function(s, bound) >= V:
				if t >= T:
					samples.append(1)
				else:
					samples.append(0)

			estimate = np.mean(samples)
			ci = self.Z * np.sqrt((estimate*(1-estimate)) / len(samples))
			history.append((estimate, ci, total_cost))

			if verbose:
				print(history[-1])

		return history

	def MLSS_dfs(self, T, V, state, bound, dur_cnt, idx, splits, boundaries):
		s, t = state
		cost = 0
		path = 0
		hits = 0
		for i in range(splits):
			if idx == len(boundaries)-1:
				q = self.simulate(T - t, boundaries[idx], s, bound)
				cost += len(q)
				#print(idx, len(q), dur_cnt+self.durability_function(q, bound))
				path += 1
				# if len(q) > 0 and q[-1] >= boundaries[idx]:
				# if dur_cnt + self.durability_function(q, bound) >= boundaries[idx]:
				if q[-1] >=  boundaries[idx]:
					hits += 1
			else:
				q = self.simulate(T - t, boundaries[idx], s, bound)
				cost += len(q)
				path += 1
				s.extend(q)
				new_state = s[-len(self.params):]
				new_cnt = self.durability_function(q, bound)
				#print(idx, len(q), dur_cnt+new_cnt)
				# if len(q) > 0 and q[-1] >= boundaries[idx]:
				if new_cnt >= boundaries[idx]:
					c,p,h = self.MLSS_dfs(T, V, (new_state, t+len(q)), bound, \
						new_cnt, idx+1, splits, boundaries)
					cost += c
					path += p
					hits += h
		return cost, path, hits


	# simulation of one root path using MLSS
	def MLSS_root_path(self, T, V, initial_state, bound, splits, boundaries):
		total_cost = 0
		hits = 0
		path_cnt = 0

		idx = 0
		s = self.simulate(T, boundaries[idx], initial_state, bound)
		t = len(s)
		total_cost += t
		dur_cnt = self.durability_function(s, bound)
		#print(idx, t, dur_cnt)

		initial_state = list(initial_state)
		initial_state.extend(s)
		state = initial_state[-len(self.params):]

		if dur_cnt >= V:
			return hits, total_cost, path_cnt

		if dur_cnt >= boundaries[idx]:
			c, p, h = self.MLSS_dfs(T, V, (state, t), bound, dur_cnt, \
				idx+1, splits, boundaries)
			total_cost += c
			path_cnt += p
			hits += h

		return hits, total_cost, path_cnt

	def MLSS(self, T, V, splits, bound, boundaries, initial_state, ci_threshold, verbose=False):
		total_cost = 0
		estimate = 1
		ci = 1

		m = len(boundaries)

		target_hits = 0
		root_paths = 0

		root_path_hits = []

		history = []
		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(5):
				h, c, p = self.MLSS_root_path(T, V, initial_state, \
					bound, splits, boundaries)
				target_hits += h
				total_cost += c
				root_path_hits.append(h)

			# estimation
			estimate = target_hits / (len(root_path_hits) * splits ** (m-1))
			estimated_var = np.var(root_path_hits) / (len(root_path_hits) * splits ** (2*(m-1)))
			ci = self.Z * np.sqrt(estimated_var)
			# print(root_path_hits[-1])
			history.append((estimate, ci, total_cost))

			if verbose:
				print(history[-1])

		return history

	def MLSS_dfs_hybrid(self, V, state, idx, splits, t_boundaries, v_boundaries, bound):
		s, t = state
		cost = 0
		hits = 0

		for i in range(splits):
			if idx == len(t_boundaries)-1:
				q = self.simulate(np.sum(t_boundaries)-t, v_boundaries[idx], s, bound)
				cost += len(q)
				# if len(q) >= t_boundaries[idx]:
				if t+len(q) >= np.sum(t_boundaries):
					hits += 1
			else:
				q = self.simulate(np.sum(t_boundaries[:idx+1])-t, v_boundaries[idx], s, bound)
				cost += len(q)
				s.extend(q)
				new_state = s[-len(self.params):]
				# if len(q) >= t_boundaries[idx]:
				if q[-1] >= V:
					continue
				c,h = self.MLSS_dfs_hybrid(V, (new_state, t+len(q)), idx+1, \
					splits, t_boundaries, v_boundaries, bound)
				cost += c
				hits += h
		return cost, hits


	def MLSS_hybrid_root_path(self, T, V, splits, initial_state, t_boundaries, v_boundaries, bound=0):
		total_cost = 0
		total_hits = 0

		idx = 0
		s = self.simulate(t_boundaries[idx], v_boundaries[idx], initial_state, bound)
		t = len(s)
		total_cost += t

		initial_state = list(initial_state)
		initial_state.extend(s)
		state = initial_state[-len(self.params):]

		if s[-1] >= V:
			return total_cost, total_hits

		# if t >= t_boundaries[idx]:
			# v_idx = min(len(v_boundaries)-1, np.searchsorted(v_boundaries, s[-1], side='right'))
		c, h = self.MLSS_dfs_hybrid(V, (state,t), idx+1, splits, t_boundaries, v_boundaries, bound)
		total_cost += c
		# self.hits[v_idx].append(h)
		total_hits += h
		# else:
		# 	self.hits[len(v_boundaries)-1].append(0)

		return total_cost, total_hits

	def MLSS_hybrid(self, T, V, splits, initial_state, t_boundaries, v_boundaries, ci_threshold):
		total_cost = 0
		estimate = 1
		ci = 1

		m = len(t_boundaries)

		root_paths = 0
		root_path_hits = []

		# self.initialize(t_boundaries, v_boundaries)

		history = []

		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(5):
				root_paths += 1
				c, h = self.MLSS_hybrid_root_path(T, V, splits, initial_state, t_boundaries, v_boundaries)
				total_cost += c
				root_path_hits.append(h)

			# # sanity check
			# flag = 0
			# for k in self.hits:
			# 	if len(self.hits[k]) == 0:
			# 		flag = 1
			# if flag:
			# 	continue

			# estimation
			estimate = 0
			estimated_var = 0

			estimate = np.sum(root_path_hits) / (len(root_path_hits) * splits ** (m-1))
			estimated_var = np.var(root_path_hits) / (len(root_path_hits) * splits ** (2*(m-1)))

			# # merge value partition with the same splitting ratio
			# split_dict = defaultdict(list)
			# for k in self.hits:
			# 	split_dict[splits[k]].extend(self.hits[k])

			# assert root_paths == np.sum([len(split_dict[v]) for v in split_dict])

			# for v in split_dict:
			# 	estimate += np.sum(split_dict[v]) / (root_paths * v ** (m-1))

			# if len(split_dict) == 1:
			# 	v = list(split_dict.keys())[0]
			# 	estimated_var = np.var(split_dict[v]) / (root_paths * v ** (2*(m-1)))
			# else:
			# 	for v in split_dict:
			# 		if v < 0:
			# 			continue
			# 		# estimated_var += len(self.hits[k]) * np.var(self.hits[k]) / (root_paths * splits[k] ** (m-1)) ** 2
			# 		p_k = len(split_dict[v]) / root_paths
			# 		avg_hits_k = np.mean(split_dict[v])
			# 		var_hits_k = np.var(split_dict[v])

			# 		estimated_var += (var_hits_k * p_k) / (root_paths * v ** (2*(m-1)))
			# 		estimated_var += (avg_hits_k**2 * p_k * (1-p_k)) / (root_paths * v ** (2*(m-1)))

			# print(estimate, estimated_var)
			ci = self.Z * np.sqrt(estimated_var)
			history.append((estimate, ci, total_cost))

		return history


def AR_model_test():
	model = AR_model((-0.3, 0.4, 0.5), 5, (0, 2), 1.96)

	# model.simulate(200, 30, (1,2,3), 20, plot=True)

	srs_history = model.SRS(200, 25, (1,2,3), 16, 0.01, False)
	print(srs_history[-1])

	mlss_history = model.MLSS(200, 25, 2, 16, [15, 22, 25], (1,2,3), 0.01, False)
	print(mlss_history[-1])

	# model.MLSS_root_path(200, 25, [1,2,3], 16, 3, [8, 12, 16])


	# srs_avg_est = []
	# srs_avg_cost = []
	# for i in range(10):
	# 	srs_history = model.SRS(200, 20, (1,2,3), 0.01)
	# 	srs_avg_est.append(srs_history[-1][0])
	# 	srs_avg_cost.append(srs_history[-1][2])
	# print(np.mean(srs_avg_est), np.mean(srs_avg_cost))

	# mlss_avg_est = []
	# mlss_avg_cost = []
	# for i in range(10):
	# 	mlss_history = model.MLSS(200, 20, 3, [15, 20], (1,2,3), 0.01)
	# 	mlss_avg_est.append(mlss_history[-1][0])
	# 	mlss_avg_cost.append(mlss_history[-1][2])
	# print(np.mean(mlss_avg_est), np.mean(mlss_avg_cost))
	
	# h,c,p,f = model.MLSS_root_path(100, 16, (5,6,7), 3, [10,15,16])
	# print(h,c,p,f)
	# model.simulate(200, 20, (1,2,3), True)
	# srs = model.SRS(200, 19, (5,6,7), 0.01)
	# print(srs[-1])

def AR_hybrid_test():
	model = AR_model((-0.3, 0.4, 0.5), 5, (0, 2), 1.96)

	T = 500
	V = 19.5
	t_splits = [400, 100]
	v_splits = [15, 19.5]

	srs_history = model.SRS(T, V, (1,2,3), 0, 0.01, False)
	print(srs_history[-1])

	mlss_history = model.MLSS(T, V, 3, 0, v_splits, (1,2,3), 0.01, False)
	print(mlss_history[-1], 1-mlss_history[-1][0])

	mlss_hybrid = model.MLSS_hybrid(T, V, 3, (1,2,3), t_splits, v_splits, 0.01)
	print(mlss_hybrid[-1])

if __name__ == '__main__':
	# AR_model_test()
	AR_hybrid_test()