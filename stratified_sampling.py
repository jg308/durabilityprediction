import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import random
import copy
from collections import defaultdict

from multilevel_spliting import *
from cluster_sampling import *

EXPAND_RATIO = 5

def dfs(state, idx, split_levels, T, level_count, verbose=False):
	s1, s2, t = state
	cost = 0
	path = 0
	for i in range(EXPAND_RATIO):
		if idx == len(split_levels)-1:
			q1, q2 = tandem_queue(s1, s2, T-t, 0.5, 2, 2, split_levels[idx])
			cost += len(q2)
			path += 1
			# if verbose:
			# 	print('[{}]'.format(idx), t+len(q2), q2)
			if len(q2) > 0 and q2[-1] >= split_levels[idx]:
				level_count[split_levels[idx]][0] += 1
			else:
				level_count[split_levels[idx]][1] += 1
		else:
			q1, q2 = tandem_queue(s1, s2, T-t, 0.5, 2, 2, split_levels[idx])
			cost += len(q2)
			path += 1
			# if verbose:
			# 	print('[{}]'.format(idx), t+len(q2), q2)
			if len(q2) > 0 and q2[-1] >= split_levels[idx]:
				level_count[split_levels[idx]][0] += 1
				c, p = dfs((q1[-1], q2[-1], t+len(q2)), idx+1, split_levels, T, level_count, verbose)
				cost += c
				path += p
			else:
				level_count[split_levels[idx]][1] += 1
	return cost, path
 
# multi-level-splitting-depth-first
def MLSS_dfs(split_levels, T, level, batch_size, verbose=False):
	level_count = {}
	for sl in split_levels:
		# count of success and failures in each level
		level_count[sl] = [0,0]
	
	total_cost = 0
	path_cnt = 0

	history = []
	success_path_cnt = []

	previous_success_cnt = 0

	for i in range(batch_size):
		idx = 0
		s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_levels[0])
		t = len(s2)
		total_cost += t
		path_cnt += 1
		# if verbose:
		# 	print('[{}]'.format(idx), t, s2, total_cost)
		if s2[-1] >= split_levels[idx]:
			level_count[split_levels[idx]][0] += 1
			c, p = dfs((s1[-1], s2[-1], t), idx+1, split_levels, T, level_count, verbose)
			total_cost += c
			path_cnt += p
		else:
			level_count[split_levels[idx]][1] += 1

		# if verbose:
		# 	print('sample cost:{}, total path:{}'.format(total_cost, path_cnt))
		# print(level_count)

		estimate = 1
		estimate_v2 = 1

		estimate_v2 = level_count[level][0] / ((i+1) * EXPAND_RATIO**(len(split_levels)-1))

		success_path_cnt.append(level_count[level][0] - previous_success_cnt)
		previous_success_cnt = level_count[level][0]

		# variance of the estimator
		var = np.var(success_path_cnt) / EXPAND_RATIO**(2*len(split_levels)-1)

		ci = 1.96*np.sqrt(var/(i+1))

		for key in level_count:
			if np.sum(level_count[key]) == 0:
				estimate = 0
				break
			else:
				estimate *= level_count[key][0] / np.sum(level_count[key])
		#print(i, total_cost, estimate)
		history.append((estimate, total_cost, path_cnt))
	
	if verbose:
		print(estimate, estimate_v2, ci, var, total_cost)
	return history


# multi-level-stratified sampling
def MLSS(split_levels, T, level, batch_size, ci_threshold, verbose=False):
	# print('===Start of MLSS===')
	#print('Time:{}, level:{}, ci threshold:{}, batch_size:{}'.format(\
	#	T, level, ci_threshold, batch_size))
	cluster_weights = defaultdict(int)
	level_ratio = {}
	level_states = []
	next_level_states = []
	total_cost = 0

	# create strata cluster
	for sl in split_levels:
		# intial start
		if len(level_states) == 0:
			for i in range(batch_size):
				s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, sl)
				t = len(s2)
				total_cost += t
				if s2[-1] >= sl:
					level_states.append((s1[-1], s2[-1], t))
				else:
					cluster_weights[sl] += 1
			level_ratio[sl] = (len(level_states), cluster_weights[sl])
			if verbose:
				print('success:{}({}), fail:{}({})'.format(\
					len(level_states),len(level_states)/batch_size, \
					cluster_weights[sl], cluster_weights[sl]/batch_size))
		# start from level_states
		else:
			rdm_idx = np.random.choice(range(len(level_states)), batch_size)
			for idx in rdm_idx:
				q1, q2, t = level_states[idx]
				s1, s2 = tandem_queue(q1, q2, T-t, 0.5, 2, 2, sl)
				total_cost += len(s2)
				if len(s2) > 0 and s2[-1] >= sl:
					next_level_states.append((s1[-1], s2[-1], t + len(s2)))
				else:
					cluster_weights[sl] += 1
			level_states = copy.deepcopy(next_level_states)
			next_level_states = []
			level_ratio[sl] = (len(level_states), cluster_weights[sl])
			if verbose:
				print('success:{}({}), fail:{}({})'.format(\
					len(level_states),len(level_states)/batch_size, \
					cluster_weights[sl], cluster_weights[sl]/batch_size))

	#print('intial cost:{}'.format(total_cost))

	cluster_weights[0] = len(level_states)
	# adjust relative weight
	relative_weights = {}
	success = level_ratio[split_levels[0]][0]
	fail = level_ratio[split_levels[0]][1]
	for sl in split_levels[1:]:
		success = success * level_ratio[sl][0] / batch_size
	success = success / batch_size
	fail = 1 - success

	# print(cluster_weights)
	# print(level_ratio)
	#print(success, fail)
	# random sampling on the last strata

	history = []
	sample_labels = []
	estimate = 0
	ci = 1
	while ci > ci_threshold:
		rdm_idx = np.random.choice(range(len(level_states)), 50)
		for idx in rdm_idx:
			q1, q2, t = level_states[idx]
			assert q2 == split_levels[-1]
			s1, s2 = tandem_queue(q1, q2, T-t, 0.5, 2, 2, level)
			total_cost += len(s2)
			if len(s2) > 0 and s2[-1] >= level:
				sample_labels.append(1)
			else:
				sample_labels.append(0)
		estimate = success * np.mean(sample_labels)
		ci = 1.96*np.sqrt(success*np.var(sample_labels)/len(sample_labels))
		history.append((estimate, ci, total_cost))
		if verbose:
			print(estimate, ci, total_cost)

	#print(estimate, ci, total_cost)
	return estimate, ci, total_cost, history

def cost_plot(srs, mlss_list, ground_truth, ci):
	srs_est = [v[0] for v in srs]
	srs_ci = [v[1] for v in srs]
	srs_cost = [v[2] for v in srs]

	plt.hlines(ground_truth, 0, srs_cost[-1], color='black', label='ground truth')
	plt.fill_between(range(srs_cost[-1]), ground_truth+ci, ground_truth-ci, color='black', alpha=0.2)

	plt.plot(srs_cost, srs_est, color='b', label='SRS')
	plt.fill_between(srs_cost, [item[0] - item[1] for item in zip(srs_est, srs_ci)],\
		[item[0] + item[1] for item in zip(srs_est, srs_ci)], color='b', alpha=0.2)

	for idx, mlss in enumerate(mlss_list):
		mlss_est = [v[0] for v in mlss]
		mlss_ci = [v[1] for v in mlss]
		mlss_cost = [v[2] for v in mlss]

		plt.plot(mlss_cost, mlss_est, label='MLSS-{}'.format(idx+1))
		plt.fill_between(mlss_cost, [item[0] - item[1] for item in zip(mlss_est, mlss_ci)],\
			[item[0] + item[1] for item in zip(mlss_est, mlss_ci)], alpha=0.2)

	plt.legend()
	plt.show()

def abs_err_with_std(timeframe, boundary, ground_truth, trails):
	filename = 'data/MLSS_SRS_{}_{}_{}_{}.py'.format(timeframe, boundary, EXPAND_RATIO, trails)
	f = open(filename, 'w')
	print('write to file {}'.format(filename))
	srs_path = {}
	mlss_path = {}
	for i in range(trails):
		if i % 50 == 0:
			print('\r {}'.format(i), end='')
		mlss_history = MLSS_dfs([int(boundary/2)+1, boundary], timeframe, boundary, 500)
		mlss_path[i] = mlss_history
		srs_history = SRS_v2(timeframe, boundary, 2000)
		srs_path[i] = srs_history

	# average on multiple runs
	srs_min_len = np.min([len(srs_path[p]) for p in srs_path])
	mlss_min_len = np.min([len(mlss_path[p]) for p in mlss_path])

	print(srs_min_len, mlss_min_len)

	# prepare plot
	srs_avg_cost = []
	srs_avg_abs_err = []
	srs_avg_est_std = []
	for idx in range(srs_min_len):
		srs_avg_cost.append(\
			(np.mean([srs_path[p][idx][1] for p in srs_path]),\
			np.min([srs_path[p][idx][1] for p in srs_path]),\
			np.max([srs_path[p][idx][1] for p in srs_path])))
		srs_avg_abs_err.append(\
			(np.mean([np.abs(srs_path[p][idx][0]-ground_truth) for p in srs_path]),\
			np.min([np.abs(srs_path[p][idx][0]-ground_truth) for p in srs_path]),\
			np.max([np.abs(srs_path[p][idx][0]-ground_truth) for p in srs_path])))
		srs_avg_est_std.append(np.std([srs_path[p][idx][0] for p in srs_path]))

	mlss_avg_cost = []
	mlss_avg_path_cnt = []
	mlss_avg_abs_err = []
	mlss_avg_est_std = []
	for idx in range(mlss_min_len):
		mlss_avg_cost.append(\
			(np.mean([mlss_path[p][idx][1] for p in mlss_path]),\
			np.min([mlss_path[p][idx][1] for p in mlss_path]),\
			np.max([mlss_path[p][idx][1] for p in mlss_path])))
		mlss_avg_abs_err.append(\
			(np.mean([np.abs(mlss_path[p][idx][0]-ground_truth) for p in mlss_path]),\
			np.min([np.abs(mlss_path[p][idx][0]-ground_truth) for p in mlss_path]),\
			np.max([np.abs(mlss_path[p][idx][0]-ground_truth) for p in mlss_path])))
		mlss_avg_est_std.append(np.std([mlss_path[p][idx][0] for p in mlss_path]))
		mlss_avg_path_cnt.append(np.mean([mlss_path[p][idx][2] for p in mlss_path]))

	print('from matplotlib import pyplot as plt', file=f)
	#print(srs_min_len, file=f)
	print('srs_avg_cost={}'.format(srs_avg_cost), file=f)
	print('srs_avg_abs_err={}'.format(srs_avg_abs_err), file=f)
	print('srs_avg_est_std={}'.format(srs_avg_est_std), file=f)
	#print(mlss_min_len, file=f)
	print('mlss_avg_path_cnt={}'.format(mlss_avg_path_cnt), file=f)
	print('mlss_avg_cost={}'.format(mlss_avg_cost), file=f)
	print('mlss_avg_abs_err={}'.format(mlss_avg_abs_err), file=f)
	print('mlss_avg_est_std={}'.format(mlss_avg_est_std), file=f)

	f.close()


def average_cost_estimation_plot(timeframe, level, split_levels, ci_threshold, ground_truth):
	print(timeframe, level, split_levels, ci_threshold)
	srs_path = {}
	mlss_path = {}

	for i in range(500):
		if i % 50 == 0:
			print(i)
		_,_,_,srs_past = SRS(timeframe, level, ci_threshold)
		_,_,_,mlss_past = MLSS(split_levels, timeframe, level, 1000, ci_threshold)
		# print(len(srs_past), len(mlss_past))
		srs_path[i] = srs_past
		mlss_path[i] = mlss_past

	# average on multple traces
	srs_min_len = np.min([len(srs_path[p]) for p in srs_path])
	mlss_min_len = np.min([len(mlss_path[p]) for p in mlss_path])

	# prepare plot
	srs_avg_cost = []
	srs_avg_est = []
	for idx in range(srs_min_len):
		srs_avg_cost.append(np.mean([srs_path[p][idx][2] for p in srs_path]))
		srs_avg_est.append((np.mean([srs_path[p][idx][0] for p in srs_path]),\
			np.min([srs_path[p][idx][0] for p in srs_path]),np.max([srs_path[p][idx][0] for p in srs_path])))

	mlss_avg_cost = []
	mlss_avg_est = []
	for idx in range(mlss_min_len):
		mlss_avg_cost.append(np.mean([mlss_path[p][idx][2] for p in mlss_path]))
		mlss_avg_est.append((np.mean([mlss_path[p][idx][0] for p in mlss_path]),\
			np.min([mlss_path[p][idx][0] for p in mlss_path]),np.max([mlss_path[p][idx][0] for p in mlss_path])))

	# print(srs_avg_cost)
	# print(srs_avg_est)
	# print(mlss_avg_cost)
	# print(mlss_avg_est)

	plt.plot(srs_avg_cost, [item[0] for item in srs_avg_est], color='b', label='SRS')
	plt.fill_between(srs_avg_cost, [item[1] for item in srs_avg_est], [item[2] for item in srs_avg_est], color='b', alpha=0.2)
	plt.plot(mlss_avg_cost, [item[0] for item in mlss_avg_est], color='r', label='MLSS')
	plt.fill_between(mlss_avg_cost, [item[1] for item in mlss_avg_est], [item[2] for item in mlss_avg_est], color='r', alpha=0.2)

	v,c = ground_truth
	plt.hlines(v, srs_avg_cost[0], srs_avg_cost[-1], color='black', label='ground truth')
	plt.fill_between(range(int(srs_avg_cost[0]), int(srs_avg_cost[-1])), v-c, v+c, color='black', alpha=0.2)

	plt.show()




def average_cost_comparison(timeframe, level, split_levels, ci_threshold):
	srs_cost = []
	mlss_cost = []
	for i in range(10):
		_,_,srs_c,_ = SRS(timeframe, level, ci_threshold)
		_,_,mlss_c,_ = MLSS(split_levels, timeframe, level, 1000, ci_threshold)
		srs_cost.append(srs_c)
		mlss_cost.append(mlss_c)

	plt.bar(1, np.mean(srs_cost), yerr=np.std(srs_cost), label='SRS')
	plt.bar(2, np.mean(mlss_cost), yerr=np.std(mlss_cost), label='MLSS')
	plt.legend()
	plt.show()
	return np.mean(srs_cost), np.std(srs_cost), np.mean(mlss_cost), np.std(mlss_cost)

def mlss_variance_test(timeframe, boundary, batch_size, trails):
	MLSS_dfs([int(boundary/2)+1, boundary], timeframe, boundary, batch_size, True)
	estimate_variance = []
	for i in range(trails):
		if i % 10 == 0:
			print('\r {}'.format(i), end='')
		history = MLSS_dfs([int(boundary/2)+1, boundary], timeframe, boundary, batch_size)
		estimate_variance.append(history[-1][0])

	print(np.var(estimate_variance))


if __name__ == '__main__':
	timeframe = 500
	boundary = 15
	ci_threshold = 0.05
	# ground_truth = 0.07848

	print(timeframe, boundary)
	# ground_truth,ci = simple_random_sampling(0.01, 50000, timeframe, boundary, plot_id=None)
	# _,_,_,SRS_history = SRS(timeframe, boundary, ci_threshold)
	# #_,_,_,MLSS_history_4 = MLSS([4], timeframe, boundary, 1000, ci_threshold, verbose=False)
	# _,_,_,MLSS_history_6 = MLSS([6], timeframe, boundary, 1000, ci_threshold, verbose=False)
	# # _,_,_,MLSS_history_8 = MLSS([8], timeframe, boundary, 1000, ci_threshold, verbose=False)
	# # _,_,_,MLSS_history_10 = MLSS([10], timeframe, boundary, 1000, ci_threshold, verbose=False)
	# _,_,_,MLSS_history_12 = MLSS([6], timeframe, boundary, 2000, ci_threshold, verbose=False)
	# cost_plot(SRS_history, [MLSS_history_6, MLSS_history_12], ground_truth, ci)
	# mlss_history_1 = MLSS_dfs([6, 12, 15], timeframe, boundary, 1, True)
	mlss_history_2 = MLSS_dfs([10, 15], timeframe, boundary, 100, True)
	# mlss_history_3 = MLSS_dfs([12,18], timeframe, boundary, 2000)
	# _,_,_,srs_history = SRS(timeframe, boundary, ci_threshold, True)
	# plt.plot([item[1] for item in mlss_history_1], [np.abs(item[0]-ground_truth) for item in mlss_history_1], label='MLSS-1')
	# plt.plot([item[1] for item in mlss_history_2], [np.abs(item[0]-ground_truth) for item in mlss_history_2], label='MLSS-2')
	# plt.plot([item[1] for item in mlss_history_3], [np.abs(item[0]-ground_truth) for item in mlss_history_3], label='MLSS-3')
	# plt.plot([item[-1] for item in srs_history], [np.abs(item[0]-ground_truth) for item in srs_history], label='SRS')
	# plt.legend()
	# plt.ylim(ymin=0)
	# plt.show()

	# mlss_variance_test(timeframe, boundary, 100, 100)

	# abs_err_with_std(timeframe, boundary, ground_truth, 500)

	#average_cost_comparison(timeframe, boundary, [6, 12], ci_threshold)

	#average_cost_estimation_plot(timeframe, boundary, [6, 9], ci_threshold, (ground_truth,ci))
