import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import scipy.stats as ss
import seaborn as sns
import random

NOISE_SIGMA = 5
MAX_STEP = 50
POPULATION = 1000
PROB = 0.8

def ind_white_noises(size, sigma):
	return np.random.normal(0, sigma, size).reshape(POPULATION,1)

def random_walk_test(trails, topk_boundary, visual=False):
	print('random walk')
	# define model params
	#np.random.seed(30)
	X = np.random.permutation(POPULATION).reshape(POPULATION, 1)
	rank_X = ss.rankdata(np.transpose(X))
	target = -1
	for i in range(POPULATION):
		#if rank_X[i] <= topk_boundary:
		if rank_X[i] <= 10:
			target = i
			break
	print(target, rank_X[target])
	initial_rank = rank_X[target]

	coff = np.identity(POPULATION)

	initial_state = X

	FHT_dist = []

	possible_worlds_rank = defaultdict(list)

	# start simulation
	for i in range(trails):
		count = 0
		step = 0
		# recover
		X = initial_state
		target_rank = []
		target_rank.append(initial_rank)
		while step < MAX_STEP:
			errors = ind_white_noises(POPULATION, NOISE_SIGMA)
			X = np.matmul(coff, X) + errors
			rank_X = ss.rankdata(np.transpose(X), method='ordinal')
			target_rank.append(rank_X[target])
			if rank_X[target] <= topk_boundary:
				count+=1
			# else:
			# 	break
			step+=1
		FHT_dist.append(step/MAX_STEP)
		possible_worlds_rank[len(target_rank)].append(target_rank)

	#print(possible_worlds_rank)

	if visual == True:
		key = random.sample(list(possible_worlds_rank.keys()), k=9)
		for idx, duration in enumerate(key):
			for line in possible_worlds_rank[duration]:
				plt.subplot(3,3,idx+1)
				plt.hlines(topk_boundary, 0, duration, color='r')
				plt.plot(line)
				plt.title(duration)
		plt.tight_layout()
		plt.show()

	#print(FHT_dist)
	# print(sum(FHT_dist)/len(FHT_dist))
	sns.distplot(FHT_dist, hist=True, kde=True)
	plt.show()

	return possible_worlds_rank, sum(FHT_dist)/len(FHT_dist)


# VAR(1) model with n time series
def var_test(trails, topk_boundary, target, visual=False):
	print('VAR(1)')
	# define model params
	np.random.seed(30)
	#X = np.random.permutation(POPULATION).reshape(POPULATION, 1)
	X = np.array(range(1,POPULATION+1)).reshape(POPULATION,1)
	#print(X)
	rank_X = ss.rankdata(np.transpose(X))
	# target = -1
	# for i in range(POPULATION):
	# 	if rank_X[i] <= 2*topk_boundary and np.random.uniform(0,1,1)[0] < PROB:
	# 	#if rank_X[i] <= 10:
	# 		target = i
	# 		break
	print(target, rank_X[target])

	# get a p-sample of the time series object
	# sample_vector = []
	# for i in range(POPULATION):
	# 	if i == target or np.random.uniform(0,1,1)[0] >= PROB:
	# 		sample_vector.append(1)
	# 	else:
	# 		sample_vector.append(10000000)
	# sample_vector = np.array(sample_vector)

	initial_rank = rank_X[target]
	coff = np.identity(POPULATION) * np.random.rand(POPULATION, POPULATION)
	#print(coff)
	constant = []
	for i in range(POPULATION):
		constant.append(X[i,0] * (1 - coff[i,i]))
	constant = np.array(constant).reshape(POPULATION, 1)

	initial_state = X
	
	
	FHT_dist = []
	sample_FHT_dist = []
	level_value = defaultdict(list)
	level_objects = defaultdict(set)

	possible_worlds_rank = defaultdict(list)

	# start simulation
	for i in range(trails):
		count = 0
		sample_count = 0
		step = 0
		# recover
		X = initial_state
		target_rank = []
		target_rank.append(initial_rank)
		while step < MAX_STEP:
			errors = ind_white_noises(POPULATION, NOISE_SIGMA)
			X = np.matmul(coff, X) + constant + errors
			rank_X = ss.rankdata(np.transpose(X), method='ordinal')
			level_value[i].append(X[np.where(rank_X==topk_boundary)[0][0],0])
			for r in range(5*topk_boundary, 6*topk_boundary):
				level_objects[r].add(np.where(rank_X==r)[0][0])
			#sample_X = np.transpose(X) * sample_vector
			#sample_rank_X = ss.rankdata(sample_X, method='ordinal')
			sample_rank_X = ss.rankdata(np.transpose(X)[:,:60], method='ordinal')
			target_rank.append(rank_X[target])
			if rank_X[target] <= topk_boundary:
				count+=1
			if sample_rank_X[target] <= topk_boundary:
				sample_count+=1
			# else:
			# 	break
			step+=1
		FHT_dist.append(count/MAX_STEP)
		sample_FHT_dist.append(sample_count/MAX_STEP)
		#print(target_rank)
		possible_worlds_rank[len(target_rank)].append(target_rank)

	#print(possible_worlds_rank)

	if visual == True:
		key = random.sample(list(possible_worlds_rank.keys()), k=9)
		for idx, duration in enumerate(key):
			for line in possible_worlds_rank[duration]:
				plt.subplot(3,3,idx+1)
				plt.hlines(topk_boundary, 0, duration, color='r')
				plt.plot(line)
				plt.title(duration)
		plt.tight_layout()
		plt.show()

	#print(FHT_dist)
	#print(sum(FHT_dist)/len(FHT_dist))
	for level in level_objects:
		print(level_objects[level])
	print(np.mean(FHT_dist),np.mean(sample_FHT_dist))
	plt.subplot(3,1,1)
	sns.distplot(FHT_dist, hist=True, kde=True)
	plt.subplot(3,1,2)
	sns.distplot(sample_FHT_dist, hist=True, kde=True)
	plt.subplot(3,1,3)
	plt.plot(level_value[1])
	plt.plot(level_value[3])
	plt.plot(level_value[5])
	plt.show()

	return possible_worlds_rank, sum(FHT_dist)/len(FHT_dist)

def get_rank_distribution_per_time(possible_worlds, avg_duration, random_seed=False):
	if random_seed is True:
		candidates_keys = [t for t in possible_worlds if t >= avg_duration and len(possible_worlds[t]) > 20]
		avg_duration = random.sample(candidates_keys, k=1)[0]
	print(avg_duration, len(possible_worlds[avg_duration]))


	plt.boxplot(np.transpose(np.array(possible_worlds[avg_duration])).tolist())
	# for t in range(avg_duration):
	# 	for v in [line[t] for line in possible_worlds[avg_duration]]:
	# 		plt.scatter(t, v)
		# avg_rank = np.mean([line[t] for line in possible_worlds[avg_duration]])
		# plt.scatter(t, avg_rank)
		# values = [line[t] for line in possible_worlds[avg_duration]]
		# sns.distplot(values, hist=True, kde=True, label='t:{}'.format(t))
	#plt.legend()
	plt.show()

def get_rank_distribution_overall(possible_worlds, avg_duration):
	ranks = defaultdict(list)
	
	for duration in possible_worlds:
		if duration > 3*avg_duration:
			continue
		for path in possible_worlds[duration]:
			for t, v in enumerate(path):
				ranks[t].append(v)
	# plt.hlines(15, 0, 3*avg_duration, color='r')
	# plt.vlines(avg_duration, 0, 30, color='r')
	plt.boxplot([ranks[t] for t in range(int(avg_duration)) if t in ranks.keys()])
	plt.show()

if __name__ == '__main__':
	worlds, avg_duration = var_test(1000, 10, 12, False)
	#worlds, avg_duration = random_walk_test(2000, 100, False)
	#get_rank_distribution_per_time(worlds, int(avg_duration), True)
	#get_rank_distribution_overall(worlds, avg_duration)
	