import random
import numpy as np
import copy
import time
from collections import defaultdict
from matplotlib import pyplot as plt

T_THRESHOLD = 500
RATIO = 0.8

class cp_model:
	# U(t) = u + c*t - S(t), where S(t) is a compound poisson process
	# with uniform jump distribution [lo, hi]
	def __init__(self, params, c, Z):
		self.lamda, self.lo, self.hi = params
		self.c = c
		self.Z = Z
		# maintain the intermidiate counters for each level
		self.crossing_prob = defaultdict(list)
	
	def poisson_arrival(self, T):
		arrival_times = set()
		_arrival_time = 0
		while True:
			#Get the next probability value from Uniform(0,1)
			p = np.random.uniform(0,1,1)[0]

			#Plug it into the inverse of the CDF of Exponential(_lamnbda)
			_inter_arrival_time = -np.log(p)/self.lamda

			#Add the inter-arrival time to the running sum
			_arrival_time = _arrival_time + _inter_arrival_time

			arrival_times.add(int(_arrival_time))

			if _arrival_time > T:
				break

		return arrival_times

	def simulate(self, T, V, state, base=0, plot=False):
		supply, demand = state

		sequence = []

		if T == 0:
			return sequence, (supply, demand), 0
		
		arrival_instants = self.poisson_arrival(T)

		start = time.time()
		for t in range(T):
			if t+base >= T_THRESHOLD * RATIO:
				if np.random.uniform(0, 1, 1)[0] < 0.005:
					supply += 200
					# supply += self.c
				else:
					supply += self.c
			else:
				supply += self.c
			if t in arrival_instants:
				demand += np.random.uniform(self.lo, self.hi, 1)[0]
				# if t+start <= T_THRESHOLD * RATIO:
				# 	demand += np.random.uniform(self.lo, self.hi, 1)[0]
				# else:
				# 	# demand += np.random.uniform(self.lo * 10, self.hi * 10, 1)[0]
				# 	demand += np.random.uniform(-self.hi * 30, self.hi * 30, 1)[0]

			sequence.append(supply - demand)

			if supply - demand >= V:
				break
		end = time.time()

		if plot is True:
			plt.plot(sequence)
			max_diff = 0
			time_diff = 0
			value = 0
			for t in range(len(sequence) - 1):
				if sequence[t+1] - sequence[t] > max_diff:
					max_diff = sequence[t+1] - sequence[t]
					time_diff = t
					value = sequence[t]
			plt.title('max diff:{}, time:{}, value:{}'.format(max_diff, time_diff, value))
			plt.show()

		return sequence, (supply, demand), end-start

	def SRS(self, T, V, state, quality, q_type, ground_truth=0):
		total_cost = 0
		total_time = 0
		estimate = 0
		quality_measure = 1
		samples = []

		history = []
		while quality_measure > quality or estimate == 0 or estimate == 1 or quality_measure == 0:
			for i in range(10):
				s, _, simulation_time = self.simulate(T, V, state, 0)
				t = len(s)
				total_cost += t
				total_time += simulation_time
				# if t > 0 and s2[-1] >= V:
				if t >= T:
					# not ruin
					samples.append(0)
				else:
					# ruin
					samples.append(1)

			estimate = np.mean(samples)

			if q_type == 0:
				quality_measure = self.Z * np.sqrt((estimate*(1-estimate))/len(samples))
			else:
				quality_measure = np.sqrt((estimate*(1-estimate))/len(samples)) / ground_truth

			history.append((estimate, quality_measure, total_cost, total_time))

		return history

	def SRS_by_time(self, T, V, state, time_limit):
		total_cost = 0
		total_time = 0
		estimate = 0
		quality_measure = 1
		samples = []

		while total_cost <= time_limit:
			s, _, simulation_time = self.simulate(T, V, state, 0)
			t = len(s)
			total_cost += t
			total_time += simulation_time
			# if t > 0 and s2[-1] >= V:
			if t >= T:
				# not ruin
				samples.append(0)
			else:
				# ruin
				samples.append(1)

		estimate = np.mean(samples)

		return estimate

	def MLSS_dfs(self, T, V, state, idx, splits, v_boundaries):
		s, t = state
		cost = 0
		hits = 0
		
		for i in range(splits):
			if idx == len(v_boundaries)-1:
				q, last_state, simulation_time = self.simulate(T-t, v_boundaries[idx], s, t)
				cost += len(q)
				if len(q) > 0 and q[-1] >= v_boundaries[idx]:
					hits += 1
			else:
				q, last_state, simulation_time = self.simulate(T-t, v_boundaries[idx], s, t)
				cost += len(q)
				if len(q) > 0 and q[-1] >= v_boundaries[idx]:
					v_idx = np.searchsorted(v_boundaries, q[-1], side='right')
					if v_idx >= len(v_boundaries):
						hits += 1
					else:
						c,h = self.MLSS_dfs(T, V, (last_state, t+len(q)), v_idx, splits, v_boundaries)
						cost += c
						hits += h
		return cost, hits

	def MLSS_root_path(self, T, V, splits, initial_state, v_boundaries):
		total_cost = 0
		total_hits = 0

		idx = 0
		
		s, last_state, simulation_time = self.simulate(T, v_boundaries[idx], initial_state, 0)
		t = len(s)
		total_cost += t
		
		if s[-1] >= v_boundaries[idx]:
			v_idx = np.searchsorted(v_boundaries, s[-1], side='right')

			if v_idx >= len(v_boundaries):
				return total_cost, total_hits
			
			c, h = self.MLSS_dfs(T, V, (last_state,t), v_idx, splits, v_boundaries)
			total_cost += c
			total_hits += h

		return total_cost, total_hits

	def MLSS(self, T, V, splits, initial_state, v_boundaries, time_limit):
		total_cost = 0
		estimate = 1
		ci = 1

		m = len(v_boundaries)

		root_paths = 0
		root_path_hits = []

		while total_cost <= time_limit:
			root_paths += 1
			c, h = self.MLSS_root_path(T, V, splits, initial_state, v_boundaries)
			total_cost += c
			root_path_hits.append(h)

		# estimation
		estimated_var = 0

		estimate = np.sum(root_path_hits) / (len(root_path_hits) * splits ** (m-1))
		
		return estimate

	def general_MLSS_dfs(self, T, V, state, idx, splits, boundaries):


		s, t = state
		skip_flag = 0
		cost = 0
		time = 0
		hits = 0

		for i in range(splits):
			q, last_state, simulation_time = self.simulate(T-t, boundaries[idx], s, t)
			cost += len(q)
			# if cost > 0:
			# 	print('idx:{}, value:{}'.format(idx, q[-1]))
			time += simulation_time
			if idx >= len(boundaries)-1:
				if len(q) > 0 and q[-1] >= boundaries[idx]:
					hits += 1
			else:
				if len(q) > 0 and q[-1] >= boundaries[idx]:
					hits += 1
					v_idx = np.searchsorted(boundaries, q[-1], side='right')
					for level in range(idx+1, v_idx):
						skip_flag += 1
						self.crossing_prob[level].append(1)
					if v_idx >= len(boundaries):
						continue
					c,st, flag = self.general_MLSS_dfs(T, V, (last_state, t+len(q)), v_idx, \
						splits, boundaries)
					cost += c
					time += st
					skip_flag += flag
		self.crossing_prob[idx].append(hits/splits)
		# print(idx, hits)
		return cost, time, skip_flag


	def general_MLSS_root_path(self, T, V, splits, boundaries, initial_state):
		total_cost = 0
		total_time = 0
		idx = 0

		skip_flag = 0

		s,last_state, st = self.simulate(T, boundaries[idx], initial_state, 0)
		t = len(s)
		total_cost += t
		total_time += st

		if s[-1] >= boundaries[idx]:
			# print('====root path===')
			self.crossing_prob[0].append(1)

			v_idx = np.searchsorted(boundaries, s[-1], side='right')
			# print('value:{}, level:{}'.format(s[-1], v_idx))
			for level in range(idx+1, v_idx):
				skip_flag += 1
				self.crossing_prob[level].append(1)

			if v_idx >= len(boundaries):
				return total_cost, total_time, skip_flag

			c, st, flag = self.general_MLSS_dfs(T, V, (last_state, t), v_idx, splits, boundaries)
			total_cost += c
			total_time += st
			skip_flag += flag

		return total_cost, total_time, skip_flag


	def general_MLSS(self, T, V, initial_state, splits, boundaries, time_limit):
		level_skippings = 0
		self.crossing_prob.clear()

		total_cost = 0
		total_time = 0
		total_root_paths = 0
		m = len(boundaries)

		while total_cost <= time_limit:
			tc, tt, skips = self.general_MLSS_root_path(T, V, splits, boundaries, initial_state)
			total_root_paths += 1
			total_cost += tc
			total_time += tt
			if skips > 0:
				level_skippings += 1
		
		# print('level skippings:{}%'.format(level_skippings / total_root_paths))
		# estimation
		
		estimate = 1
		estimate *= float(np.sum(self.crossing_prob[0])) / total_root_paths
		for level in range(1, m):
		
			if len(self.crossing_prob[level]) == 0:
				estimate *= 0
				break
			else:
				estimate *= np.mean(self.crossing_prob[level])

		return estimate, float(level_skippings) / total_root_paths

def simulation_plot():
	T = 500
	V = 3000
	payment = 4.5
	splits = 3

	model = cp_model((0.8, 5, 10), payment, 1.96)

	model.simulate(T, V, (15, 0), 0, True)

def general_mlss_test(T, V, boundaries):
	# T = 500
	# V = 600
	# boundaries = [200, 500, 600]
	time_limit = 100000
	print('T:{}, V:{}, Levels:{}, time limit:{}'.format(T, V, boundaries, time_limit))
	payment = 4.5
	splits = 3
	trials = 100

	model = cp_model((0.8, 5, 10), payment, 1.96)

	# model.simulate(T, V, (15, 0), 0, True)

	srs_result = []
	mlss_result = []
	basic_result = []
	skip_ratios = []

	for i in range(trials):
		basic_mlss = model.MLSS(T, V, 3, (15, 0), boundaries, time_limit)
		basic_result.append(basic_mlss)

	print('basic mlss estimate:{}, std:{}'.format(np.mean(basic_result), np.std(basic_result)))

	for i in range(trials):
		srs = model.SRS_by_time(T, V, (15, 0), time_limit)
		srs_result.append(srs)

	print('srs estimate:{}, std:{}'.format(np.mean(srs_result), \
		np.std(srs_result)))

	for i in range(trials):
		mlss, skip_ratio = model.general_MLSS(T, V, (15, 0), 3, boundaries, time_limit)
		mlss_result.append(mlss)
		skip_ratios.append(skip_ratio)
	
	print('mlss estimate:{}, std:{}, skip ratio:{}'.format(np.mean(mlss_result), \
		np.std(mlss_result), np.mean(skip_ratios)))


if __name__ == '__main__':
	general_mlss_test(500, 1000, [400, 900, 1000])
	# simulation_plot()

