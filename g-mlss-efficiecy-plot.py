from matplotlib import pyplot as plt
import numpy as np

FONT_SIZE=16

def g_mlss_efficiency_plot():
    # # cpp
    # srs_steps = [803647.93,2149240.72]
    # srs_steps_std = [83674,249214]
    # srs_time = [6.027769658565521,15.751836652755737]
    # srs_time_std = [0.65,1.698]
    # g_mlss_steps = [452869.63265306124,952805.2222222222]
    # g_mlss_steps_std = [67980.59940630538,126111.40388482677]
    # g_mlss_sim_time = [3.8912257719039918,7.972232830524445]
    # g_mlss_sim_time_std = [0.4771199487632845,0.9996385396862023]
    # g_mlss_eval_time = [1.3578371167182923,4.2647865295410154]
    # g_mlss_eval_time_std = [0.783139852254183,2.2232897339360105]

    srs_steps = [2149240.72,46160775.2]
    srs_steps_std = [249214,4843256.028347496]
    srs_time = [15.751836652755737,345.57292478084565]
    srs_time_std = [1.698,37.90097057617947]
    g_mlss_steps = [581193.28,3382531.38]
    g_mlss_steps_std = [87473.7109065438,392994.41123257164]
    g_mlss_sim_time = [5.206364488601684,27.26259816646576]
    g_mlss_sim_time_std = [0.7777115128997295,3.1981255769245096]
    g_mlss_eval_time = [3.261156105995178,30.130046253204345]
    g_mlss_eval_time_std = [1.7378556575666297,18.65671026806411]

    # queue
    q_srs_steps = [3026198.52,17475955.6]
    q_srs_steps_std = [300728.3925096359,2051558.5556722092]
    q_srs_time = [16.729022822380067,102.27857484817505]
    q_srs_time_std = [2.52938,12.618052366437768]
    q_g_mlss_steps = [712562.36,2103438.84]
    q_g_mlss_steps_std = [156903.1991925926,388427.58311630547]
    q_g_mlss_sim_time = [4.939040117263794,12.84720636844635]
    q_g_mlss_sim_time_std = [1.097257934205148,2.346727487678461]
    q_g_mlss_eval_time = [3.851124997138977,16.04712254524231]
    q_g_mlss_eval_time_std = [2.065248355682007,9.013126944712724]

    # set width of bar
    barWidth = 0.3
    # Set position of bar on X axis
    r1 = np.arange(len(srs_steps))
    r2 = [x + barWidth for x in r1]

    plt.bar(r1, srs_steps, yerr=srs_steps_std, width=barWidth, label='SRS', color='#0066cc')
    plt.bar(r2, g_mlss_steps, yerr=g_mlss_steps_std, width=barWidth, label='g-MLSS', color='red')

    plt.xticks(r1 + barWidth/2, ['Tiny Query','Rare Query'], fontsize=FONT_SIZE)
    plt.ylabel('total number of simulation steps', fontsize=FONT_SIZE)
    plt.legend(loc=2, fontsize=FONT_SIZE,frameon=False)

    plt.show()

    plt.bar(r1, srs_time, yerr=srs_time_std, width=barWidth, label='SRS', color='#0066cc')
    plt.bar(r2, [item[0] + item[1] for item in zip(g_mlss_sim_time, g_mlss_eval_time)], yerr=g_mlss_eval_time_std, width=barWidth, label='Bootstrap Eval', color='green')
    plt.bar(r2, g_mlss_sim_time, width=barWidth, label='g-MLSS', color='red')

    plt.xticks(r1 + barWidth/2, ['Tiny Query','Rare Query'], fontsize=FONT_SIZE)
    plt.ylabel('query time (second)', fontsize=FONT_SIZE)
    plt.legend(loc=2, fontsize=FONT_SIZE,frameon=False)

    plt.show()

    plt.bar(r1, q_srs_steps, yerr=q_srs_steps_std, width=barWidth, label='SRS', color='#0066cc')
    plt.bar(r2, q_g_mlss_steps, yerr=q_g_mlss_steps_std, width=barWidth, label='g-MLSS', color='red')

    plt.xticks(r1 + barWidth/2, ['Tiny Query','Rare Query'], fontsize=FONT_SIZE)
    plt.ylabel('total number of simulation steps', fontsize=FONT_SIZE)
    plt.legend(loc=2, fontsize=FONT_SIZE,frameon=False)

    plt.show()

    plt.bar(r1, q_srs_time, yerr=q_srs_time_std, width=barWidth, label='SRS', color='#0066cc')
    plt.bar(r2, [item[0] + item[1] for item in zip(q_g_mlss_sim_time, q_g_mlss_eval_time)], yerr=q_g_mlss_eval_time_std, width=barWidth, label='Bootstrap Eval', color='green')
    plt.bar(r2, q_g_mlss_sim_time, width=barWidth, label='g-MLSS', color='red')

    plt.xticks(r1 + barWidth/2, ['Tiny Query','Rare Query'], fontsize=FONT_SIZE)
    plt.ylabel('query time (second)', fontsize=FONT_SIZE)
    plt.legend(loc=2, fontsize=FONT_SIZE,frameon=False)

    plt.show()

def g_mlss_greedy_partition_plot():
    cpp_srs_time = [15.751836652755737,345.57292478084565]
    cpp_g_mlss_sim_time_opt = [5.206364488601684,27.26259816646576]
    cpp_g_mlss_eval_time_opt = [3.261156105995178,30.130046253204345]
    cpp_g_mlss_sim_time_greedy = [6.33739972114563,35.89484763145447]
    cpp_g_mlss_eval_time_greedy = [2.660614252090454,25.041144609451294]
    cpp_g_mlss_search_time = [4.414382696151733,23.249648809432983]

    q_srs_time = [16.729022822380067,102.27857484817505]
    q_g_mlss_sim_time_opt = [4.939040117263794,12.84720636844635]
    q_g_mlss_eval_time_opt = [3.851124997138977,16.04712254524231]
    q_g_mlss_sim_time_greedy = [5.426138162612915,15.543535947799683]
    q_g_mlss_eval_time_greedy = [3.9268574714660645,16.633843898773193]
    q_g_mlss_search_time = [2.294968843460083,3.812818765640259]

     # set width of bar
    barWidth = 0.15
    # Set position of bar on X axis
    r1 = np.arange(len(cpp_srs_time))
    r2 = [x + barWidth + 0.02 for x in r1]
    r3 = [x + barWidth + 0.02 for x in r2]

    # plt.bar(r1, [1,1], edgecolor='black', width=barWidth, label='SRS', color='#0066cc')
    # plt.bar(r2, [(item[0]+item[1])/item[2] for item in zip(cpp_g_mlss_sim_time_opt, cpp_g_mlss_eval_time_opt, cpp_srs_time)], edgecolor='black', width=barWidth, label='Bootstrap Eval', color='green')
    # plt.bar(r2, [item[0]/item[1] for item in zip(cpp_g_mlss_sim_time_opt,cpp_srs_time)], edgecolor='black', width=barWidth, label='MLSS-OPT', color='red')
    # plt.bar(r3, [(item[0]+item[1]+item[2])/item[3] for item in zip(cpp_g_mlss_sim_time_greedy, cpp_g_mlss_eval_time_greedy, cpp_g_mlss_search_time, cpp_srs_time)], edgecolor='black', width=barWidth, label='MLSS-G-Partition', color='brown')
    # plt.bar(r3, [(item[0]+item[1])/item[2] for item in zip(cpp_g_mlss_sim_time_greedy, cpp_g_mlss_eval_time_greedy, cpp_srs_time)], edgecolor='black', width=barWidth, color='green')
    # plt.bar(r3, [item[0]/item[1] for item in zip(cpp_g_mlss_sim_time_greedy, cpp_srs_time)], edgecolor='black', width=barWidth, label='MLSS-G', color='yellow')
    # plt.text(r1[0]-0.1, 1.02, '2149240',fontsize=FONT_SIZE)
    # plt.text(r1[1]-0.1, 1.02, '46160775',fontsize=FONT_SIZE)

    plt.bar(r1, [1,1], edgecolor='black', width=barWidth, label='SRS', color='#0066cc')
    plt.bar(r2, [(item[0]+item[1])/item[2] for item in zip(q_g_mlss_sim_time_opt, q_g_mlss_eval_time_opt, q_srs_time)], edgecolor='black', width=barWidth, label='Bootstrap Eval', color='green')
    plt.bar(r2, [item[0]/item[1] for item in zip(q_g_mlss_sim_time_opt,q_srs_time)], edgecolor='black', width=barWidth, label='MLSS-OPT', color='red')
    plt.bar(r3, [(item[0]+item[1]+item[2])/item[3] for item in zip(q_g_mlss_sim_time_greedy, q_g_mlss_eval_time_greedy, q_g_mlss_search_time, q_srs_time)], edgecolor='black', width=barWidth, label='MLSS-G-Partition', color='brown')
    plt.bar(r3, [(item[0]+item[1])/item[2] for item in zip(q_g_mlss_sim_time_greedy, q_g_mlss_eval_time_greedy, q_srs_time)], edgecolor='black', width=barWidth, color='green')
    plt.bar(r3, [item[0]/item[1] for item in zip(q_g_mlss_sim_time_greedy, q_srs_time)], edgecolor='black', width=barWidth, label='MLSS-G', color='yellow')
    plt.text(r1[0]-0.1, 1.02, '3026198',fontsize=FONT_SIZE)
    plt.text(r1[1]-0.1, 1.02, '17475955',fontsize=FONT_SIZE)



    plt.xticks(r1 + barWidth/2, ['Tiny Query','Rare Query'], fontsize=FONT_SIZE)
    plt.ylabel('ratio to SRS baseline (%)', fontsize=FONT_SIZE)
    plt.legend(loc='upper center', fontsize=FONT_SIZE,frameon=False, ncol=2, labelspacing=0.05,columnspacing=0.5)
    plt.tight_layout()
    plt.ylim(0,1.5)

    plt.show()


if __name__ == '__main__':
    # g_mlss_efficiency_plot()
    g_mlss_greedy_partition_plot()