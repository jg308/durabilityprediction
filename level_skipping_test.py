import random
import numpy as np
import copy
import time
from collections import defaultdict
from matplotlib import pyplot as plt

class AR_model:
	def __init__(self, params, constant, error, Z):
		self.params = params
		self.c = constant
		self.mu, self.sigma = error
		self.Z = Z
		self.path_hits = defaultdict(list)
		self.counter = defaultdict(int)
		self.level_counters = defaultdict(list)

	def noise(self, T):
		return np.random.normal(self.mu, self.sigma, T)

	def simulate(self, T, V, initital_state):
		sequence = []
		white_noise = self.noise(T)
		state = list(initital_state)
		for t in range(T):
			#print(state, white_noise[t])
			x = self.c
			for item in zip(self.params, state):
				x += item[0] * item[1]
			x += white_noise[t]
			sequence.append(x)
			state = state[1:]
			state.append(x)
			if x >= V:
				break

		return sequence

	def SRS(self, T, V, initial_state, ci_threshold):
		total_cost = 0
		estimate = 0
		ci = 1
		samples = []

		history = []

		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(10):
				s = self.simulate(T, V, initial_state)
				t = len(s)
				total_cost += t
				if t < T:
					samples.append(1)
				else:
					samples.append(0)

			estimate = np.mean(samples)
			ci = self.Z * np.sqrt((estimate*(1-estimate)) / len(samples))
			history.append((estimate, ci, total_cost))

		return history

	def MLSS_dfs(self, T, V, state, idx, path_id, splits, boundaries):
		s, t = state
		cost = 0
		hits = 0

		if s[-1] < V:
			for i in range(splits):
				if idx == len(boundaries)-1:
					q = self.simulate(T - t, boundaries[idx], s)
					cost += len(q)
					
					if len(q) > 0 and q[-1] >=  boundaries[idx]:
						v_idx = np.searchsorted(boundaries, q[-1], side='right')
						self.counter[path_id + str(v_idx)] += 1
						hits += 1
				else:
					# TO DO for more general cases
					print('Should not be here!')
				
		return cost, hits

	def MLSS_root_path(self, T, V, initial_state, splits, boundaries):
		total_cost = 0

		idx = 0
		path_id = str(idx)

		s = self.simulate(T, boundaries[idx], initial_state)
		t = len(s)
		total_cost += t

		initial_state = list(initial_state)
		initial_state.extend(s)
		state = initial_state[-len(self.params):]

		

		if s[-1] >= boundaries[idx]:
			v_idx = np.searchsorted(boundaries, s[-1], side='right')
			path_id += str(v_idx)
			self.counter[path_id] += 1

			if v_idx == 2:
				self.path_hits[path_id].append(1)
			elif v_idx == 1:
				c, h = self.MLSS_dfs(T, V, (state, t), idx+1, path_id, splits, boundaries)
				self.path_hits[path_id + str(len(boundaries))].append(h)
				total_cost += c
			else:
				print('should not be here!')
			

		return total_cost

	def MLSS(self, T, V, splits, boundaries, initial_state, ci_threshold):
		self.path_hits.clear()
		self.counter.clear()

		total_root_paths = 0
		total_cost = 0
		estimate = 1
		ci = 1

		m = len(boundaries)

		history = []
		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(10):
				c = self.MLSS_root_path(T, V, initial_state, splits, boundaries)
				total_cost += c
				total_root_paths += 1

			# estimation
			estimate = 0
			for key in self.path_hits:
				estimate += np.sum(self.path_hits[key]) / (total_root_paths * splits ** (len(key) - 2))
			
			if self.counter['01'] == 0 or self.counter['02'] == 0:
				continue

			estimated_var = 0
			p_skip = self.counter['02'] / total_root_paths
			p_no_skip = self.counter['01'] / total_root_paths
			p_12 = self.counter['012'] / (splits * self.counter['01'])
			estimated_var += p_skip * (1 - p_skip) / total_root_paths
			estimated_var += p_12 ** 2 * p_no_skip * (1 - p_no_skip) / total_root_paths
			estimated_var += (p_no_skip * np.var(self.path_hits['012'])) / (total_root_paths * splits ** 2)

			ci = self.Z * np.sqrt(estimated_var)
			history.append((estimate, ci, total_cost))
			# if total_root_paths % 1000 == 0:
			# 	# print(p_skip, p_no_skip, p_12)
			# 	print(history[-1])

		return history

	def new_MLSS_dfs(self, T, V, state, idx, splits, boundaries):
		s, t = state
		cost = 0
		hits = 0

		if s[-1] < V:
			for i in range(splits):
				if idx == len(boundaries)-1:
					q = self.simulate(T - t, boundaries[idx], s)
					cost += len(q)
					
					if len(q) > 0 and q[-1] >=  boundaries[idx]:
						v_idx = np.searchsorted(boundaries, q[-1], side='right')
						hits += 1
				else:
					# TO DO for more general cases
					print('Should not be here!')
			self.level_counters[idx+1].append(hits/splits)
				
		return cost, hits

	def new_MLSS_root_path(self, T, V, initial_state, splits, boundaries):
		total_cost = 0

		idx = 0

		s = self.simulate(T, boundaries[idx], initial_state)
		t = len(s)
		total_cost += t

		initial_state = list(initial_state)
		initial_state.extend(s)
		state = initial_state[-len(self.params):]

		

		if s[-1] >= boundaries[idx]:
			self.level_counters[1].append(1)

			v_idx = np.searchsorted(boundaries, s[-1], side='right')

			if v_idx == 2:
				self.level_counters[2].append(1)
			elif v_idx == 1:
				c, h = self.new_MLSS_dfs(T, V, (state, t), idx+1, splits, boundaries)
				total_cost += c
			else:
				print('should not be here!')
			

		return total_cost

	def new_MLSS(self, T, V, splits, boundaries, initial_state):

		total_root_paths = 0
		total_cost = 0
		estimate = 1

		history = []

		while True:
			for i in range(50):
				c = self.new_MLSS_root_path(T, V, initial_state, splits, boundaries)
				total_cost += c
				total_root_paths += 1

			# estimation
			estimate = 1
			estimate *= np.sum(self.level_counters[1]) / total_root_paths
			estimate *= np.mean(self.level_counters[2])

			history.append(estimate)

			if len(history) > 1000:
				break

		return history		

	def debug_info(self):
		print('counter: ', self.counter)
		print('hits: ', self.path_hits)
	
	def num_level_skipping(self):
		return self.counter['02']



def test():
	T = 500
	V = 28
	initial_state = (1,2,3)
	splits = 3
	boundaries = [26, 28]
	ci_threshold = 0.01

	model = AR_model((-0.2, 0.4, 0.5), 5, (0, 3), 1.96)
	srs = model.SRS(T, V, initial_state, ci_threshold)
	print(srs[-1])
	mlss = model.MLSS(T, V, splits, boundaries, initial_state, ci_threshold)
	print(mlss[-1])
	new_mlss = model.new_MLSS(T, V, splits, boundaries, initial_state)
	print(new_mlss[-1])
	# for i in range(100):
	# 	model.MLSS_root_path(T, V, initial_state, splits, boundaries)
	# model.debug_info()

def var_test():
	T = 500
	V = 26.5
	initial_state = (1,2,3)
	splits = 3
	boundaries = [24, 26.5]
	ci_threshold = 0.03

	print(T, V, boundaries, ci_threshold)

	model = AR_model((-0.3, 0.4, 0.5), 5, (0, 3), 1.96)

	srs_estimate = []
	srs_cost = []
	mlss_estimate = []
	mlss_cost = []

	level_skipping = []

	for i in range(100):
		print('\r{}'.format(i), end = '')
		srs = model.SRS(T, V, initial_state, ci_threshold)
		srs_estimate.append(srs[-1][0])
		srs_cost.append(srs[-1][2])
		mlss = model.MLSS(T, V, splits, boundaries, initial_state, ci_threshold)
		mlss_estimate.append(mlss[-1][0])
		mlss_cost.append(mlss[-1][2])
		level_skipping.append(model.num_level_skipping())

	print('srs:', np.mean(srs_estimate), np.std(srs_estimate), np.mean(srs_cost))
	print('mlss:', np.mean(mlss_estimate), np.std(mlss_estimate), np.mean(mlss_cost))
	print('level skipping: ', np.mean(level_skipping))

if __name__ == '__main__':
	# var_test()
	# test()

