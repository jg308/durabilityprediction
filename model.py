import random
import numpy as np

def poisson_arrival(T, lamda):
	arrival_times = set()
	_arrival_time = 0
	while True:
		#Get the next probability value from Uniform(0,1)
		p = np.random.uniform(0,1,1)[0]

		#Plug it into the inverse of the CDF of Exponential(_lamnbda)
		_inter_arrival_time = -np.log(p)/lamda

		#Add the inter-arrival time to the running sum
		_arrival_time = _arrival_time + _inter_arrival_time

		arrival_times.add(int(_arrival_time))

		if _arrival_time > T:
			break

	return arrival_times


def tandem_queue(queue_1, queue_2, T, lamda, mu_1, mu_2, level, plot=False):

	arrival_instants = poisson_arrival(T, lamda)

	q1_sequence = []
	q2_sequence = []

	deque_1_time = int(np.random.exponential(mu_1,1)[0])+1
	deque_2_time = int(np.random.exponential(mu_2,1)[0])+1

	for t in range(T):
		
		# join queue 1
		if t in arrival_instants:
			queue_1 += 1
		if queue_1 == 0:
			deque_1_time += 1
		# quit queue 1 and join queue 2
		if queue_1 > 0 and t == deque_1_time:
			queue_1 -= 1
			queue_2 += 1
			deque_1_time += int(np.random.exponential(mu_1,1)[0])+1
		# quit queue 2
		if queue_2 == 0:
			deque_2_time += 1
		if queue_2 > 0 and t == deque_2_time:
			queue_2 -= 1
			deque_2_time += int(np.random.exponential(mu_2,1)[0])+1

		q1_sequence.append(queue_1)
		q2_sequence.append(queue_2)

		if queue_2 >= level:
			break
		
	if plot:
		print(q2_sequence)
		#plt.plot(q1_sequence, color='r', label='Queue 1')
		plt.plot(q2_sequence, color='black', label='Queue 2')
		plt.hlines(level, 0, T, color='r')
		plt.legend()
		plt.show()

	return q1_sequence, q2_sequence