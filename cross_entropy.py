import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

SIGMA = 2

def white_noises(sigma, T):
    return np.random.normal(0, sigma, T)

def weighted_noises(parameters):
    noises = []
    for mu in parameters:
        noises.append(np.random.normal(mu, SIGMA, 1)[0])
    return noises

def error_sum_test(errors):
    return np.sum(errors)

def simple_random_sampling_test(ci_threshold, batch_size, T, sum_threshold):
    samples = []
    mean = 0
    ci = 1000
    while ci > ci_threshold:
        for i in range(batch_size):
            epsilon = white_noises(SIGMA, T)
            error_sum = error_sum_test(epsilon)
            if error_sum >= sum_threshold:
                samples.append(1)
            else:
                samples.append(0)
        mean = np.mean(samples)
        ci = 1.96 * np.sqrt(np.var(samples)) / np.sqrt(len(samples))
       	#print(mean, ci)
    print(mean, ci)
    # print('sample count:{}'.format(len(samples)))
    return mean, len(samples)

def relative_likelihood(epsilon, parameters):
    # return np.exp((1 / (2 * SIGMA**2)) * \
    #               np.sum([(epsilon[i] - parameters[i])**2 - epsilon[i]**2 for i in range(len(epsilon))]))
    constant = 1 / (2 * SIGMA**2)
    new_part = 0
    old_part = 0
    for i in range(len(epsilon)):
        #print((epsilon[i] - parameters[i])**2, epsilon[i]**2)
        new_part += (epsilon[i] - parameters[i])**2
        old_part += epsilon[i]**2
    #print(constant * (new_part - old_part))
    #print(-constant * old_part, -constant * new_part)
    return np.exp(constant * (new_part - old_part))

def cross_entropy_update_test(rho, opt_parameters, T, sum_threshold):
    # step 1 : generate samples
    samples = []
    generated_samples = []
    for i in range(500):
        epsilon = weighted_noises(opt_parameters)
        generated_samples.append(epsilon)
        samples.append(error_sum_test(epsilon))
    # step 2 : find (1 - rho) quantile
    sorted_samples = sorted(samples)
    position = int((1 - rho) * (len(sorted_samples) + 1))
    level_hat = sorted_samples[position]
    #print(sorted_samples, position, level_hat)
    
    # step 3 : update sampling distribution
    for j in range(T):
        relative_weight = relative_likelihood(generated_samples[i], opt_parameters)
        up = np.sum([relative_weight * generated_samples[i][j] \
              for i in range(len(samples)) if samples[i] >= level_hat])
        down = np.sum([relative_weight for i in range(len(samples)) if samples[i] >= level_hat])
        opt_parameters[j] = up / down
    
    #print(level_hat)
    stop = 0
    if level_hat >= sum_threshold:
        stop = 1
    return stop

def importance_sampling_test(ci_threshold, batch_size, params, T, sum_threshold):
    samples = []
    generated_errors = []
    mean = 0
    ci = 1
    while ci > ci_threshold:
        for i in range(batch_size):
            epsilon = weighted_noises(params)
            generated_errors.append(epsilon)
            error_sum = error_sum_test(epsilon)
            #print(error_sum)
            if error_sum >= sum_threshold:
                samples.append(1)
            else:
                samples.append(0)
        weights = [relative_likelihood(generated_errors[idx], params) for idx in range(len(samples))]
        weighted_samples = [samples[idx] * relative_likelihood(generated_errors[idx], params) \
                       for idx in range(len(samples))]
        #print(weighted_samples)
        mean = np.mean(weighted_samples) # / np.mean(weights)
        #ci = 1.96 * np.sqrt(np.sum([weights[idx]**2 * (-mean) for idx in range(len(samples))]))
        ci = 1.96 * np.sqrt(np.var(weighted_samples)) / np.sqrt(len(weighted_samples))
    print(mean, ci)
    # print('sample count : {}'.format(len(samples)))
    return mean, len(samples)

def cross_entropy_method(rho, dimension, threshold, ci_threshold, batch_size=5000):
    sample_count = 0
    dist_params = [0] * dimension
    while True:
        flag = cross_entropy_update_test(rho, dist_params, dimension, threshold)
        #print(dist_params)
        if flag == 1:
            break
    return importance_sampling_test(0.01, batch_size, dist_params, dimension, threshold)

if __name__ == '__main__':
    # srs = []
    # srs_sample_cnt = []
    # ois = []
    # ois_sample_cnt = []
    # print("======simple random sampling======")
    # for i in range(100):
    #     srs_mean, srs_samples = simple_random_sampling_test(0.01, 1000, 20, 30)
    #     srs.append(srs_mean)
    #     srs_sample_cnt.append(srs_samples)
    # print(np.mean(srs), np.sqrt(np.var(srs)), np.mean(srs_sample_cnt))

    print("======simple random sampling======")
    srs_mean, srs_samples = simple_random_sampling_test(0.01, 5000, 20, 32)

    print("======Important Sampling======")
    estimate, sample_cnt = cross_entropy_method(0.01, 20, 32, 0.01)
    

