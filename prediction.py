import random
import numpy as np
from scipy.stats import truncnorm
from collections import defaultdict
import matplotlib.pyplot as plt
# probability of failure
p = 0.2
n = 100
runs = 10000000

def int_normal_distribution(scale, range, size=100000):
	X = truncnorm(a=-range/scale, b=+range/scale, scale=scale).rvs(size=size)
	X = X.round().astype(int)
	dist = defaultdict(int)
	print(dist)
	for x in X:
		dist[x]+=1
	for x in sorted(dist.keys()):
		print("{}:{}".format(x, dist[x]))
	return dist

def expected_prediction(n):
	expected_sum = 0
	for i in range(0, n+1):
		expected_sum += i * (1-p) ** i 
	return expected_sum * p

def simulation_prediction(n, trails):
	predictions = 0
	for i in range(trails):
		length = 0
		r = random.uniform(0,1)
		while r >= 0.2:
			length += 1
			r = random.uniform(0,1)
			if length == n:
				break
		predictions += length
	return predictions / trails

def ar1_simulation_value_distribution(phi, sigma, trails, initial, boundary, limit=10):
	value_distribution = defaultdict(list)
	for i in range(trails):
		length = 0
		values = []
		x = initial
		error = np.random.normal(0, sigma, limit)
		while x < boundary and length < limit:
			value_distribution[length].append(x)
			x = phi * x + error[length]
			length += 1
			#plt.scatter(length, x)
	#plt.grid(True)
	#plt.hlines(boundary, 0, limit, color='r')
	#plt.show()
	for i in sorted(value_distribution.keys()):
		if i == 0:
			continue
		plt.subplot(limit, 1, i)
		plt.hist(value_distribution[i])
		plt.xlim([-8, 8])
	#plt.tight_layout()
	plt.show()


def ar1_simulation_prediction(phi, sigma, trails, initial, boundary, limit=300):
	error = np.random.normal(0, sigma, limit)
	print(np.max(error), np.min(error), np.max(error) / (1-phi), initial, boundary)
	idx = {}
	for i,v in enumerate(np.arange(-15, 15, 0.1)):
		idx[float(format(v, '0.1f'))] = i
	distribution = []
	FHT = 0
	count = np.empty([500,limit+1])
	for i in range(trails):
		length = 0
		values = []
		x = initial
		error = np.random.normal(0, sigma, limit)
		while x < boundary and length < limit:
			x = phi * x + error[length]
			values.append(x)
			length+=1
			count[idx[float(format(x, '0.1f'))]][length] += 1
		plt.plot(range(0, length), values)
		FHT += length
		distribution.append(length)
	plt.hlines(boundary, 0, limit, color='r')
	plt.show()
	# print(distribution)
	# plt.figure()
	# plt.imshow(count[0:210,:])
	# plt.hlines(idx[float(format(float(boundary), '0.1f'))], 0, limit, color='r')
	# plt.show()

	print(FHT / trails)

	# plt.figure()
	# plt.hist(distribution)
	# plt.show()
		#print(values)
	return FHT / trails

if __name__ == '__main__':
	# expected_length = expected_prediction(n)
	# simulation_length = simulation_prediction(n, runs)

	# print("expected: ", expected_length)
	# print("simulation: ", simulation_length)
	# int_normal_distribution(3, 10)
	#print("simulation : ", ar1_simulation_prediction(0.3, 2, 100, 0, 5))
	ar1_simulation_value_distribution(0.3, 2, 1000, 0, 5)
