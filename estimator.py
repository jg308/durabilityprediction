from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import random
import copy
from collections import defaultdict
from model import *

Z = 1.96

def SRS(T, level, ci_threshold, verbose=False):

	total_cost = 0
	estimate = 0
	ci = 1
	samples = []

	history = []
	while ci > ci_threshold or estimate == 0:
		for i in range(10):
			s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, level)
			t = len(s2)
			total_cost += t
			if t >= T and s2[-1] < level:
				samples.append(0)
			else:
				samples.append(1)

		estimate = np.mean(samples)
		ci = Z * np.sqrt((estimate*(1-estimate))/len(samples))
		
		history.append((estimate, ci, total_cost))

		if verbose and len(samples) % 100 == 0:
			print(history[-1])

	return history


def dfs(state, idx, split_levels, T, level_count, EXPAND_RATIO, verbose=False):
	s1, s2, t = state
	cost = 0
	path = 0
	for i in range(EXPAND_RATIO):
		if idx == len(split_levels)-1:
			q1, q2 = tandem_queue(s1, s2, T-t, 0.5, 2, 2, split_levels[idx])
			cost += len(q2)
			path += 1
			# if verbose:
			# 	print('[{}]'.format(idx), t+len(q2), q2)
			if len(q2) > 0 and q2[-1] >= split_levels[idx]:
				level_count[split_levels[idx]][0] += 1
			else:
				level_count[split_levels[idx]][1] += 1
		else:
			q1, q2 = tandem_queue(s1, s2, T-t, 0.5, 2, 2, split_levels[idx])
			cost += len(q2)
			path += 1
			# if verbose:
			# 	print('[{}]'.format(idx), t+len(q2), q2)
			if len(q2) > 0 and q2[-1] >= split_levels[idx]:
				level_count[split_levels[idx]][0] += 1
				c, p = dfs((q1[-1], q2[-1], t+len(q2)), idx+1, split_levels, T, level_count, EXPAND_RATIO, verbose)
				cost += c
				path += p
			else:
				level_count[split_levels[idx]][1] += 1
	return cost, path


 
# multi-level-splitting-depth-first
def MLSS_dfs(split_levels, T, level, ci_threshold, EXPAND_RATIO, verbose=False):
	level_count = {}
	for sl in split_levels:
		# count of success and failures in each level
		level_count[sl] = [0,0]
	
	total_cost = 0
	path_cnt = 0

	history = []
	success_path_cnt = []
	sample_path_cost = []

	previous_success_cnt = 0

	estimate = 1
	ci = 1

	while ci > ci_threshold or estimate == 0:
		for i in range(5):
			idx = 0
			s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_levels[0])
			t = len(s2)
			total_cost += t
			c = 0
			path_cnt += 1
			# if verbose:
			# 	print('[{}]'.format(idx), t, s2, total_cost)
			if s2[-1] >= split_levels[idx]:
				level_count[split_levels[idx]][0] += 1
				c, p = dfs((s1[-1], s2[-1], t), idx+1, split_levels, T, level_count, EXPAND_RATIO, verbose)
				total_cost += c
				path_cnt += p
			else:
				level_count[split_levels[idx]][1] += 1

			success_path_cnt.append(level_count[level][0] - previous_success_cnt)
			previous_success_cnt = level_count[level][0]
			sample_path_cost.append(t + c)

		# estimate and variance of the estimator
		estimated_var = np.var(success_path_cnt) / (len(success_path_cnt)*EXPAND_RATIO**(2*(len(split_levels)-1)))

		ci = Z * np.sqrt(estimated_var)

		estimate = 1
		for key in level_count:
			if np.sum(level_count[key]) == 0:
				estimate = 0
				break
			else:
				estimate *= level_count[key][0] / np.sum(level_count[key])

		history.append((estimate, ci, total_cost, path_cnt, \
			np.mean(sample_path_cost), np.var(success_path_cnt)))

		if verbose and len(success_path_cnt) % 100 == 0:
			print(history[-1])
	
	return history

# MLSS_dfs with a combo  with multiple parameter settings
def MLSS_dfs_combo(T, level, ci_threshold, EXPAND_RATIO, trails, verbose=False):
	# a round-robin test

	history = []
	
	total_cost = 0
	path_cnt = 0

	success_path_cnt = defaultdict(list)
	overall_success_path_cnt = []
	sample_path_cost = defaultdict(list)

	previous_success_cnt = 0

	estimate = 1
	ci = 1

	settings = []
	for i in range(level//2, level, 2):
		settings.append([i, level])

	lens = len(settings[0])

	if verbose:
		print('candidate settings:{}'.format(settings))

	level_count = {}
	for item in settings:
		# count of success and failures in each level
		for v in item:
			level_count[v] = [0,0]

	root_path = 0
	optimal_idx = -1
	while ci > ci_threshold or estimate == 0 or ci == 0:
		if root_path < trails:
			setting_idx = root_path % len(settings)
			splits = settings[setting_idx]
			# start a root path
			idx = 0
			s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, splits[0])
			t = len(s2)
			total_cost += t
			c = 0
			path_cnt += 1
			if s2[-1] >= splits[idx]:
				level_count[splits[idx]][0] += 1
				c, p = dfs((s1[-1], s2[-1], t), idx+1, splits, T, level_count, EXPAND_RATIO, verbose)
				total_cost += c
				path_cnt += p
			else:
				level_count[splits[idx]][1] += 1

			success_path_cnt[setting_idx].append(level_count[level][0] - previous_success_cnt)
			sample_path_cost[setting_idx].append(t + c)
			overall_success_path_cnt.append(level_count[level][0] - previous_success_cnt)
			previous_success_cnt = level_count[level][0]

		else:
			# find the optimal setting
			# compare performance of different setting
			if optimal_idx == -1:
				
				avg_hits = np.median([np.sum(success_path_cnt[i]) for i in range(len(settings))])
				if verbose:
					print('optimization cost:{}, median hits:{}'.format(total_cost, avg_hits))
				min_var = 100000000
				for i, item in enumerate(settings):
					expected_cost = np.mean(sample_path_cost[i])
					est_var = np.var(success_path_cnt[i])
					if verbose:
						print(item, expected_cost, est_var, expected_cost*est_var, np.sum(success_path_cnt[i]))
					if expected_cost * est_var < min_var and np.sum(success_path_cnt[i]) > avg_hits:
						min_var = expected_cost * est_var
						optimal_idx = i
				if verbose:
					print('optimal setting:{}'.format(settings[optimal_idx]))
				optimal_splits = settings[optimal_idx]

			# continue with optimal setting
			idx = 0
			s1, s2 = tandem_queue(0, 0, T, 0.5, 2, 2, optimal_splits[0])
			t = len(s2)
			total_cost += t
			c = 0
			path_cnt += 1
			if s2[-1] >= optimal_splits[idx]:
				level_count[optimal_splits[idx]][0] += 1
				c, p = dfs((s1[-1], s2[-1], t), idx+1, optimal_splits, T, level_count, EXPAND_RATIO, verbose)
				total_cost += c
				path_cnt += p
			else:
				level_count[optimal_splits[idx]][1] += 1

		if root_path % 5 == 0:
			estimate = level_count[level][0] / ((root_path+1) * EXPAND_RATIO ** (lens-1))
			estimated_var = np.var(overall_success_path_cnt) / ((root_path+1)*EXPAND_RATIO**(2*(lens-1)))
			ci = Z * np.sqrt(estimated_var)
			history.append((estimate, ci, total_cost))	

		root_path += 1

	if verbose:
		print(estimate, ci, total_cost)

	return history
