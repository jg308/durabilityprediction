from stratified_sampling import *
from multilevel_spliting import *
from cluster_sampling import *

expand_ratio = 5

# a depth-first simulation method
def adaptive_sampling_single_level(split_level, T, level, batch_size, ci_threshold, verbose=False):
	entrance_states = []
	sample_traces = []

	total_cost = 0
	seed_cnt = 0
	trace_cnt = 0

	while True:
		if seed_cnt > 5000:
			return
		q1, q2 = tandem_queue(0, 0, T, 0.5, 2, 2, split_level)
		t = len(q2)
		seed_cnt += 1
		total_cost += t
		if seed_cnt <= batch_size:
			entrance_states.append((q1[-1], q2[-1], t))
			# directly simulate this path
			for i in range(expand_ratio):
				trace_cnt += 1
				s1, s2 = tandem_queue(q1[-1], q2[-1], T-t, 0.5, 2, 2, level)
				total_cost += len(s2)
				label = 0
				if len(s2) > 0 and s2[-1] >= level:
					label = 1
				else:
					label = 0
				# whether need update
				if trace_cnt <= batch_size:
					sample_traces.append(label)
				else:
					replaced_path = np.random.choice(range(trace_cnt), 1)[0]
					if replaced_path < batch_size:
						#print(replaced_path, len(sample_traces))
						sample_traces[replaced_path] = label

		else:
			# reservoir update
			replaced_seed = np.random.choice(range(seed_cnt), 1)[0]
			if replaced_seed < batch_size:
				# purge the old seeds
				entrance_states[replaced_seed] = (q1[-1], q2[-1], t)

				for i in range(expand_ratio):
					trace_cnt += 1
					s1, s2 = tandem_queue(q1[-1], q2[-1], T-t, 0.5, 2, 2, level)
					total_cost += len(s2)
					label = 0
					if len(s2) > 0 and s2[-1] >= level:
						label = 1
					else:
						label = 0
					if trace_cnt < batch_size:
						sample_traces.append(label)
					else:
						replaced_path = np.random.choice(range(trace_cnt), 1)[0]
						if replaced_path < batch_size:
							sample_traces[replaced_path] = label


		if seed_cnt % 100 == 0:
			estimate = np.mean(sample_traces)
			ci = 1.96*np.sqrt(np.var(sample_traces)/len(sample_traces))
			print(seed_cnt, total_cost, len(sample_traces), estimate, ci)

if __name__ == '__main__':
	
	timeframe = 200
	boundary = 12
	ci_threshold = 0.01

	ground_truth,ci = simple_random_sampling(0.01, 10000, timeframe, boundary, plot_id=None)

	adaptive_sampling_single_level(8, timeframe, boundary, 2000, ci_threshold, verbose=False)
