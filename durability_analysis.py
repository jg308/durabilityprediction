import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

SIGMA = 2
Constant = 5
AR_0 = 5
AR_1 = 5
CE_batch = 10000
Phi0 = -0.5
Phi1 = 0.9

def white_noises(sigma, T):
    return np.random.normal(0, sigma, T)

def weighted_noises(parameters):
    noises = []
    for mu in parameters:
        noises.append(np.random.normal(mu, SIGMA, 1)[0])
    return noises

def AR_simulation_trace(input_0, input_1, errors, boundary = None):
    sequence = []
    x0 = input_0
    x1 = input_1
    for i in range(len(errors)):
        #print(x0, x1)
        x2 = Constant + Phi0 * x0 + Phi1 * x1 + errors[i]
        sequence.append(x2)
        if boundary is not None and x2 >= boundary:
            break
        x0 = x1
        x1 = x2
    return sequence

def simple_random_sampling(ci_threshold, batch_size, T, level):
    samples = []
    mean = 0
    ci = 1
    while ci > ci_threshold or ci == 0:
        for i in range(batch_size):
            epsilon = white_noises(SIGMA, T)
            sequence = AR_simulation_trace(AR_0,AR_1,epsilon,level)
            if len(sequence) >= T:
                samples.append(1)
            else:
                samples.append(0)
        mean = np.mean(samples)
        ci = 1.96 * np.sqrt(np.var(samples) / len(samples))
        #print(mean, ci)
    print(mean, ci)
    print('sample count:{}'.format(len(samples)))

def relative_likelihood(epsilon, parameters):
    constant = 1 / (2 * SIGMA**2)
    new_part = 0
    old_part = 0
    for i in range(len(epsilon)):
        new_part += (epsilon[i] - parameters[i])**2
        old_part += epsilon[i]**2
    return np.exp(constant * (new_part - old_part))

def cross_entropy_update(rho, opt_parameters, T, level):
    # step 1 : generate samples
    samples = []
    generated_samples = []
    for i in range(CE_batch):
        epsilon = weighted_noises(opt_parameters)
        generated_samples.append(epsilon)
        sequence = AR_simulation_trace(AR_0, AR_1, epsilon, level)
        if len(sequence) < T:
        	samples.append(len(sequence))
        else:
        	samples.append(T)

    # step 2 : find (1 - rho) quantile
    sorted_samples = sorted(samples)
    position = int((1 - rho) * (len(sorted_samples) + 1))
    level_hat = sorted_samples[position]
    #print(sorted_samples, position, level_hat)
    
    # step 3 : update sampling distribution
    for j in range(T):
        relative_weight = relative_likelihood(generated_samples[i], opt_parameters)
        up = np.sum([relative_weight * generated_samples[i][j] \
              for i in range(len(samples)) if samples[i] >= level_hat])
        down = np.sum([relative_weight for i in range(len(samples)) if samples[i] >= level_hat])
        opt_parameters[j] = up / down
    
    #print(level_hat)
    stop = 0
    if level_hat >= T:
        stop = 1
    return stop

def importance_sampling(ci_threshold, batch_size, params, T, level):
    samples = []
    generated_errors = []
    mean = 0
    ci = 1
    while ci > ci_threshold:
        for i in range(batch_size):
            epsilon = weighted_noises(params)
            generated_errors.append(epsilon)
            sequence = AR_simulation_trace(AR_0, AR_1, epsilon, level)
            if len(sequence) >= T:
                samples.append(1)
            else:
                samples.append(0)
       
        weighted_samples = [samples[idx] * relative_likelihood(generated_errors[idx], params) \
                       for idx in range(len(samples))]
        
        mean = np.mean(weighted_samples)
        ci = 1.96 * np.sqrt(np.var(weighted_samples)) / np.sqrt(len(weighted_samples))
    print(mean, ci)
    print('sample count : {}'.format(len(samples)))
    return mean, len(samples)

def cross_entropy_method(rho, dimension, threshold, ci_threshold, batch_size):
    sample_count = 0
    dist_params = [0] * dimension
    while True:
        flag = cross_entropy_update(rho, dist_params, dimension, threshold)
        #print(dist_params)
        if flag == 1:
            break
    print(dist_params)
    return importance_sampling(0.01, batch_size, dist_params, dimension, threshold)

if __name__ == '__main__':
	print("====== Simple Random Sampling ======")
	simple_random_sampling(0.01, 10000, 50, 11)

	# print("====== Importance Sampling ======")
	# cross_entropy_method(0.01, 50, 11, 0.01, 10000)