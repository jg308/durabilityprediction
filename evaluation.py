from estimator import *
import sys
import argparse
import json

def simple_test(timeframe, boundary, splits, expand_ratio, ci_threshold):
	print('===SRS===')
	srs_data = SRS(timeframe, boundary, ci_threshold)
	print(srs_data[-1])
	print('===MLSS===', splits)
	mlss_data = MLSS_dfs(splits, timeframe, boundary, ci_threshold, expand_ratio)
	print(mlss_data[-1])

def level_cost_and_variance_test(timeframe, boundary, expand_ratio, ci_threshold):
	avg_cost = []
	avg_var = []

	for l in range(int(boundary/2), boundary, 2):
		print(l, boundary)
		mlss_data = MLSS_dfs([l, boundary], timeframe, boundary, ci_threshold, expand_ratio)
		avg_cost.append(mlss_data[-1][-2])
		avg_var.append(mlss_data[-1][-1])

	print(avg_cost)
	print(avg_var)

def plan_selection_test(timeframe, boundary, ci_threshold, expand_ratio, trails_num):
	print('===SRS===')
	srs_avg_estimate, srs_avg_cost = [], []
	for i in range(trails_num):
		srs_data = SRS(timeframe, boundary, ci_threshold)
		srs_avg_estimate.append(srs_data[-1][0])
		srs_avg_cost.append(srs_data[-1][2])
	print(np.mean(srs_avg_estimate), np.std(srs_avg_estimate))
	print(np.mean(srs_avg_cost), np.std(srs_avg_cost))

	settings = []
	for i in range(boundary//2, boundary, 2):
		print([i, boundary])
		mlss_avg_est, mlss_avg_cost = [], []
		for j in range(trails_num):
			mlss_data = MLSS_dfs([i, boundary], timeframe, boundary, ci_threshold, expand_ratio)
			mlss_avg_est.append(mlss_data[-1][0])
			mlss_avg_cost.append(mlss_data[-1][2])
		print(np.mean(mlss_avg_est), np.std(mlss_avg_est))
		print(np.mean(mlss_avg_cost), np.std(mlss_avg_cost))

	mlss_avg_est, mlss_avg_cost = [], []
	print('===MLSS-Optimal===')
	for i in range(trails_num):
		mlss_data = MLSS_dfs_combo(timeframe, boundary, ci_threshold, expand_ratio, 5000, True)
		mlss_avg_est.append(mlss_data[-1][0])
		mlss_avg_cost.append(mlss_data[-1][2])
	print(np.mean(mlss_avg_est), np.std(mlss_avg_est))
	print(np.mean(mlss_avg_cost), np.std(mlss_avg_cost))

def cost_test(timeframe, boundary, splits, ci_threshold, expand_ratio):

	srs_cost = []
	mlss_cost = []

	srs_estimate = []
	mlss_estimate = []

	for i in range(100):
		if i % 10 == 0:
			print('\r{}'.format(i), end='')
		srs_data = SRS(timeframe, boundary, ci_threshold)
		srs_cost.append(srs_data[-1][2])
		srs_estimate.append(srs_data[-1][0])
		mlss_data = MLSS_dfs(splits, timeframe, boundary, ci_threshold, expand_ratio)
		mlss_cost.append(mlss_data[-1][2])
		mlss_estimate.append(mlss_data[-1][0])

	print('srs estimate:{}, std:{}'.format(np.mean(srs_estimate), np.std(srs_estimate)))
	print('srs cost:{}, std:{}'.format(np.mean(srs_cost), np.std(srs_cost)))
	print('mlss estimate:{}, std:{}'.format(np.mean(mlss_estimate), np.std(mlss_estimate)))
	print('mlss cost:{}, std:{}'.format(np.mean(mlss_cost), np.std(mlss_cost)))

def unbias_test(timeframe, boundary, splits, ci_threshold):
	# ground truth
	ground_truth_est = []
	for i in range(1000):
		if i % 10 == 0:
			print('\r compute ground truth: {}'.format(i), end='')
		srs_data = SRS(timeframe, boundary, ci_threshold)
		ground_truth_est.append(srs_data[-1][0])
	print('\n est ground truth:{}, ci:{}'.format(np.mean(ground_truth_est), 1.96*np.sqrt(np.var(ground_truth_est))))

	srs_path = {}
	mlss_path = {}

	srs_path['gt'] = np.mean(ground_truth_est)
	srs_path['ci'] = 1.96*np.sqrt(np.var(ground_truth_est))
	mlss_path['gt'] = np.mean(ground_truth_est)
	mlss_path['ci'] = 1.96*np.sqrt(np.var(ground_truth_est))


	filename_srs = 'data/SRS_{}_{}_{}_{}.json'.format(timeframe, boundary, expand_ratio, \
		'_'.join([str(v) for v in splits]))
	filename_mlss = 'data/MLSS_{}_{}_{}_{}.json'.format(timeframe, boundary, expand_ratio, \
		'_'.join([str(v) for v in splits]))


	for i in range(100):
		if i % 10 == 0:
			print('\r averaging: {}'.format(i), end='')

		mlss_path[i] = MLSS_dfs(splits, timeframe, boundary, ci_threshold, expand_ratio)
		srs_path[i] = SRS(timeframe, boundary, ci_threshold)

	with open(filename_srs, 'w') as f:
		json.dump(srs_path, f)
	print('write srs data to file {}'.format(filename_srs))
	with open(filename_mlss, 'w') as f:
		json.dump(mlss_path, f)
	print('write mlss data to file {}'.format(filename_mlss))


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('timeframe', type=int, help='length of the series')
	parser.add_argument('boundary', type=int, help='upper boundary')
	parser.add_argument('ci_threshold', type=float, help='confidence interval')
	parser.add_argument('expand_ratio', type=int, help='expand ratio for offsprings')
	parser.add_argument('trails', type=int, help='number of trails to report average')
	parser.add_argument('splits', metavar='N', type=int, nargs='+',
                    help='splitting levels')
	parser.add_argument('exp_type', type=str, help='experiment type')

	args = parser.parse_args()

	timeframe = args.timeframe
	boundary = args.boundary
	ci_threshold = args.ci_threshold
	splits = args.splits
	expand_ratio = args.expand_ratio
	trails_num = args.trails
	exp_type = args.exp_type
	
	print(timeframe, boundary, ci_threshold)

	if exp_type == 'simple':
		simple_test(timeframe, boundary, splits, expand_ratio, ci_threshold)

	if exp_type == 'ps-test':
		plan_selection_test(timeframe, boundary, ci_threshold, expand_ratio, trails_num)

	if exp_type == 'level-test':
		level_cost_and_variance_test(timeframe, boundary, expand_ratio, ci_threshold)

	if exp_type == 'unbias_test':
		unbias_test(timeframe, boundary, splits, ci_threshold)

	if exp_type == 'cost_test':
		cost_test(timeframe, boundary, splits, ci_threshold, expand_ratio)

