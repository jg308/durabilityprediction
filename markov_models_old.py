import random
import numpy as np
from collections import defaultdict
from matplotlib import pyplot as plt

class tandem_queue_model:
	def __init__(self, params, Z):
		self.lamda, self.mu_1, self.mu_2 = params
		self.Z = Z
		self.level_probs = defaultdict(list)
		self.hits = {}

	def poisson_arrival(self, T):
		arrival_times = set()
		_arrival_time = 0
		while True:
			#Get the next probability value from Uniform(0,1)
			p = np.random.uniform(0,1,1)[0]

			#Plug it into the inverse of the CDF of Exponential(_lamnbda)
			_inter_arrival_time = -np.log(p)/self.lamda

			#Add the inter-arrival time to the running sum
			_arrival_time = _arrival_time + _inter_arrival_time

			arrival_times.add(int(_arrival_time))

			if _arrival_time > T:
				break

		return arrival_times

	def simulate(self, T, V, state, plot=False):
		arrival_instants = self.poisson_arrival(T)
		deque_1_time = int(np.random.exponential(self.mu_1,1)[0])+1
		deque_2_time = int(np.random.exponential(self.mu_2,1)[0])+1

		q1_sequence = []
		q2_sequence = []

		queue_1, queue_2 = state

		for t in range(T):
		
			# join queue 1
			if t in arrival_instants:
				queue_1 += 1
			if queue_1 == 0:
				deque_1_time += 1
			# quit queue 1 and join queue 2
			if queue_1 > 0 and t == deque_1_time:
				queue_1 -= 1
				queue_2 += 1
				deque_1_time += int(np.random.exponential(self.mu_1,1)[0])+1
			# quit queue 2
			if queue_2 == 0:
				deque_2_time += 1
			if queue_2 > 0 and t == deque_2_time:
				queue_2 -= 1
				deque_2_time += int(np.random.exponential(self.mu_2,1)[0])+1

			q1_sequence.append(queue_1)
			q2_sequence.append(queue_2)

			if queue_2 >= V:
				break

		if plot:
			print(q2_sequence)
			plt.plot(q2_sequence, color='black', label='Queue 2')
			plt.hlines(V, 0, T, color='r')
			plt.legend()
			plt.show()

		return q1_sequence, q2_sequence

	def SRS(self, T, V, ci_threshold):
		total_cost = 0
		estimate = 0
		ci = 1
		samples = []

		history = []
		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(10):
				s1, s2 = self.simulate(T, V, (0,0))
				t = len(s2)
				total_cost += t
				# if t > 0 and s2[-1] >= V:
				if t >= T:
					samples.append(1)
				else:
					samples.append(0)

			estimate = np.mean(samples)
			ci = self.Z * np.sqrt((estimate*(1-estimate))/len(samples))
			
			history.append((estimate, ci, total_cost))

		return history

	def MLSS_dfs(self, T, V, state, idx, splits, boundaries):
		s1, s2, t = state
		cost = 0
		path = 0
		hits = 0
		for i in range(splits):
			if idx == len(boundaries)-1:
				q1, q2 = self.simulate(T - t, boundaries[idx], (s1, s2))
				cost += len(q2)
				#print(idx, t, q2)
				path += 1
				if len(q2) > 0 and q2[-1] >= boundaries[idx]:
					hits += 1
			else:
				q1, q2 = self.simulate(T - t, boundaries[idx], (s1, s2))
				cost += len(q2)
				path += 1
				#print(idx, t, q2)
				if len(q2) > 0 and q2[-1] >= boundaries[idx]:
					c,p,h = self.MLSS_dfs(T, V, (q1[-1], q2[-1], t+len(q2)), idx+1, splits, boundaries)
					cost += c
					path += p
					hits += h
		return cost, path, hits

	def MLSS_dfs_by_time(self, V, state, idx, splits, boundaries):
		s1, s2, t = state
		cost = 0
		path = 0
		hits = 0
		for i in range(splits):
			if idx == len(boundaries)-1:
				q1, q2 = self.simulate(boundaries[idx], V, (s1, s2))
				cost += len(q2)
				path += 1
				# print('{}-{}-{}'.format(idx, len(q2), q2))
				if len(q2) >= boundaries[idx]:
					hits += 1
			else:
				q1, q2 = self.simulate(boundaries[idx], V, (s1, s2))
				cost += len(q2)
				path += 1
				# print('{}-{}-{}'.format(idx, len(q2), q2))
				if len(q2) >= boundaries[idx]:
					c,p,h = self.MLSS_dfs_by_time(V, (q1[-1], q2[-1], t+len(q2)), idx+1, splits, boundaries)
					cost += c
					path += p
					hits += h
		return cost, path, hits

	# simulation of one root path using MLSS
	def MLSS_root_path(self, T, V, splits, boundaries):
		total_cost = 0
		hits = 0
		path_cnt = 0

		idx = 0
		s1, s2 = self.simulate(T, boundaries[idx], (0,0))
		t = len(s2)
		total_cost += t
		#print(idx, t, s2)

		if s2[-1] >= boundaries[idx]:
			c, p, h = self.MLSS_dfs(T, V, (s1[-1], s2[-1], t), idx+1, splits, boundaries)
			total_cost += c
			path_cnt += p
			hits += h

		return hits, total_cost, path_cnt

	def MLSS_root_path_by_time(self, T, V, splits, boundaries):
		total_cost = 0
		hits = 0
		path_cnt = 0

		idx = 0
		s1, s2 = self.simulate(boundaries[idx], V, (0,0))
		t = len(s2)
		total_cost += t
		# print('{}-{}-{}'.format(idx, t, s2))

		if t >= boundaries[idx]:
			c,p,h = self.MLSS_dfs_by_time(V, (s1[-1], s2[-1], t), idx+1, splits, boundaries)
			total_cost += c
			path_cnt += p
			hits += h

		return hits, total_cost, path_cnt

	def MLSS_dfs_hybrid(self, V, state, idx, v_idx, splits, t_boundaries, v_boundaries):
		s1, s2, t = state
		cost = 0
		path = 0
		hits = 0
		for i in range(splits[v_idx]):
			if idx == len(t_boundaries)-1:
				q1, q2 = self.simulate(t_boundaries[idx], V, (s1, s2))
				cost += len(q2)
				path += 1
				# print('{}-{}-{}-{}'.format(idx, len(q2), splits[v_idx], q2[-1]))
				if len(q2) >= t_boundaries[idx]:
					# new_v_idx = min(len(v_boundaries)-1, np.searchsorted(v_boundaries, q2[-1], side='right'))
					# print(idx+1, v_idx)
					# self.level_probs[idx+1][new_v_idx] += 1
					# self.hits[v_idx] += 1
					hits += 1
			else:
				q1, q2 = self.simulate(t_boundaries[idx], V, (s1, s2))
				cost += len(q2)
				path += 1
				# print('{}-{}-{}-{}'.format(idx, len(q2), splits[v_idx], q2[-1]))
				if len(q2) >= t_boundaries[idx]:
					# new_v_idx = min(len(v_boundaries)-1, np.searchsorted(v_boundaries, q2[-1], side='right'))
					# self.level_probs[idx+1][new_v_idx] += 1
					c,p,h = self.MLSS_dfs_hybrid(V, (q1[-1], q2[-1], t+len(q2)), idx+1, \
						v_idx, splits, t_boundaries, v_boundaries)
					cost += c
					path += p
					hits += h
		return cost, path, hits

	def MLSS_hybrid_root_path(self, T, V, splits, t_boundaries, v_boundaries):
		total_cost = 0
		hits = 0
		path_cnt = 0

		idx = 0
		s1, s2 = self.simulate(t_boundaries[idx], V, (0,0))
		t = len(s2)
		total_cost += t
		# self.level_probs[0][0] += 1
		# print(idx, t, s2[-1])

		if t >= t_boundaries[idx]:
			v_idx = min(len(v_boundaries)-1, np.searchsorted(v_boundaries, s2[-1], side='right'))
			# print(v_idx)
			# self.level_probs[idx+1][v_idx] += 1
			c,p,h = self.MLSS_dfs_hybrid(V, (s1[-1], s2[-1], t), idx+1, \
				v_idx, splits, t_boundaries, v_boundaries)
			total_cost += c
			path_cnt += p
			hits += h
			self.hits[v_idx].append(h)
		else:
			self.hits[len(v_boundaries)-1].append(0)

		# for k in self.level_probs:
		# 	print(self.level_probs[k])

		return hits, total_cost, path_cnt

	def MLSS_dfs_box(self, V, state, idx, splits, t_boundaries, v_boundaries):
		s1, s2, t = state
		cost = 0
		path = 0
		hits = 0
		for i in range(splits):
			if idx == len(t_boundaries)-1:
				q1, q2 = self.simulate(np.sum(t_boundaries)-t, v_boundaries[idx], (s1, s2))
				cost += len(q2)
				path += 1
				#print('{}-{}-{}-{}-{}'.format(idx, np.sum(t_boundaries)-t, v_boundaries[idx], len(q2), q2[-1]))
				if t+len(q2) >= np.sum(t_boundaries):
					#print('yes')
					hits += 1
			else:
				q1, q2 = self.simulate(np.sum(t_boundaries[:idx+1])-t, v_boundaries[idx], (s1, s2))
				cost += len(q2)
				path += 1
				if q2[-1] >= V:
					continue
				#print('{}-{}-{}-{}-{}'.format(idx, np.sum(t_boundaries[:idx+1])-t, v_boundaries[idx], len(q2), q2[-1]))
				c,p,h = self.MLSS_dfs_box(V, (q1[-1], q2[-1], t+len(q2)), idx+1, \
					splits, t_boundaries, v_boundaries)
				cost += c
				path += p
				hits += h
		return cost, path, hits

	def MLSS_box_root_path(self, T, V, splits, t_boundaries, v_boundaries):
		total_cost = 0
		hits = 0
		path_cnt = 0

		idx = 0
		s1, s2 = self.simulate(t_boundaries[idx], v_boundaries[idx], (0,0))
		t = len(s2)
		total_cost += t
		if s2[-1] >= V:
			return hits, total_cost, path_cnt 
		#print(idx, t_boundaries[idx], v_boundaries[idx], len(s2), s2)

		c,p,h = self.MLSS_dfs_box(V, (s1[-1], s2[-1], t), idx+1, \
		 splits, t_boundaries, v_boundaries)
		total_cost += c
		path_cnt += p
		hits += h

		return hits, total_cost, path_cnt

	def initialize(self, t_boundaries, v_boundaries):
		self.level_probs.clear()
		self.hits.clear()
		
		for i in range(len(t_boundaries)+1):
			self.level_probs[i] = [0] * len(v_boundaries)
		for i in range(len(v_boundaries)):
			self.hits[i] = []


	# MLSS simulation for given confidence interval
	# split type: 1 for splitting by value, 2 for splitting by time
	def MLSS(self, T, V, splits, t_boundaries, v_boundaries, ci_threshold, split_type=2):
		total_cost = 0
		estimate = 1
		ci = 1
		all_estimates = []

		if split_type == 1:
			m = len(v_boundaries)
		elif split_type == 2:
			m = len(t_boundaries)
		else:
			m = len(t_boundaries)

		target_hits = 0
		root_paths = 0

		root_path_hits = []

		# self.initialize(t_boundaries, v_boundaries)

		history = []
		while ci > ci_threshold or estimate == 0 or estimate == 1 or ci == 0:
			for i in range(5):
				if split_type == 0:
					h, c, p = self.MLSS_box_root_path(T, V, splits, t_boundaries, v_boundaries)
				if split_type == 1:
					h, c, p = self.MLSS_root_path(T, V, splits, v_boundaries)
				if split_type == 2:
					h, c, p = self.MLSS_root_path_by_time(T, V, splits, t_boundaries)
				if split_type == 3:
					h, c, p = self.MLSS_hybrid_root_path(T, V, splits, t_boundaries, v_boundaries)
				target_hits += h
				total_cost += c
				root_path_hits.append(h)
				root_paths += 1

			# estimation
			# fixed split setting
			if split_type < 3:
				estimate = target_hits / (len(root_path_hits) * splits ** (m-1))
				estimated_var = np.var(root_path_hits) / (len(root_path_hits) * splits ** (2*(m-1)))
				ci = self.Z * np.sqrt(estimated_var)
				# print(root_path_hits[-1])
				history.append((estimate, ci, total_cost))
			# hybrid split setting
			else:
				# for k in self.hits:
				# 	print(k, self.hits[k])

				# sanity check
				flag = 0
				for k in self.hits:
					if len(self.hits[k]) == 0:
						flag = 1
				if flag:
					continue

				estimate = 0
				estimated_var = 0

				# merge value partition with the same splitting ratio
				split_dict = defaultdict(list)
				for k in self.hits:
					split_dict[splits[k]].extend(self.hits[k])

				assert root_paths == np.sum([len(split_dict[v]) for v in split_dict])

				for v in split_dict:
					estimate += (np.sum(split_dict[v]) / (root_paths * v ** (m-1)))

				if len(split_dict) == 1:
					v = list(split_dict.keys())[0]
					estimated_var = np.var(split_dict[v]) / (root_paths * v ** (2*(m-1)))
				else:
					for v in split_dict:
						# estimated_var += len(self.hits[k]) * np.var(self.hits[k]) / (root_paths * splits[k] ** (m-1)) ** 2
						p_k = len(split_dict[v]) / root_paths
						avg_hits_k = np.mean(split_dict[v])
						var_hits_k = np.var(split_dict[v])

						estimated_var += (var_hits_k * p_k) / (root_paths * v ** (2*(m-1)))
						estimated_var += (avg_hits_k**2 * p_k * (1-p_k)) / (root_paths * v ** (2*(m-1)))


				# for k in self.hits:
				# 	estimate += (np.sum(self.hits[k]) / (len(root_path_hits) * splits[k] ** (m-1)))
				# 	estimated_var += len(self.hits[k]) * np.var(self.hits[k]) / (len(root_path_hits) * splits[k] ** (m-1)) ** 2

				# print(estimate, estimated_var)

				if estimated_var == 0:
					continue
				ci = self.Z * np.sqrt(estimated_var)
				history.append((estimate, ci, total_cost))

		return history


def tandem_queue_model_test():
	tandem_queue = tandem_queue_model((0.5, 2, 2), 1.96)
	# tandem_queue.simulate(500, 18, (0,0), True)
	srs = tandem_queue.SRS(500, 14, 0.01)
	print(srs[-1])
	mlss = tandem_queue.MLSS(500, 14, 3, [200, 300], 0.01, split_type=2)
	print(mlss[-1])
	mlss = tandem_queue.MLSS(500, 14, 3, [8, 14], 0.01, split_type=1)
	print(mlss[-1], 1-mlss[-1][0])
	# mlss = tandem_queue.MLSS(500, 10, 2, [5, 10], 0.01)
	# print(mlss[-1])
	# tandem_queue.MLSS_root_path(500, 18, 3, [9, 15, 18])

def tandem_queue_split_by_time_test():
	tandem_queue = tandem_queue_model((0.5, 2, 2), 1.96)
	# tandem_queue.MLSS_root_path_by_time(200, 10, 2, [50, 50, 100])
	tandem_queue.MLSS_box_root_path(500, 18, 3, [400,100], [10, 18])

def tandem_queue_hybrid_test():
	tandem_queue = tandem_queue_model((0.5, 2, 2), 1.96)
	# srs = tandem_queue.SRS(200, 12, 0.01)
	# print(srs[-1])
	# tandem_queue.initialize([50, 50, 100], [6, 12])
	# tandem_queue.MLSS_hybrid_root_path(200, 12, [1, 3], [50, 50, 100], [6, 12])
	T = 500
	V = 10
	t_splits = [200, 200, 100]
	# t_splits_2 = [300, 200]
	# t_splits_3 = [200, 300]
	# t_splits_4 = [100, 400]
	# t_splits_5 = [900, 100]
	splits = [3, 3]
	# splits_2 = [1, 1]
	# splits_3 = [5, 5]
	# splits_4 = [7, 7]
	# splits_5 = [9, 9]
	# splits_6 = [11, 11]
	# splits_7 = [13, 13]
	# splits_8 = [15, 15]

	v_splits = [7, 10, 10]
	srs = tandem_queue.SRS(T, V, 0.01)
	print(srs[-1])
	# mlss = tandem_queue.MLSS(T, V, 3, t_splits, v_splits, 0.01, split_type=2)
	# print(mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, 3, t_splits, v_splits, 0.01, split_type=1)
	# print(mlss[-1], 1-mlss[-1][0])
	# mlss = tandem_queue.MLSS(T, V, splits, t_splits, v_splits, 0.01, split_type=3)
	# print(t_splits, splits, mlss[-1])
	mlss = tandem_queue.MLSS(T, V, 3, t_splits, v_splits, 0.01, split_type=0)
	print(mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits, t_splits_2, v_splits, 0.01, split_type=3)
	# print(t_splits_2, splits, mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits, t_splits_3, v_splits, 0.01, split_type=3)
	# print(t_splits_3, splits, mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits, t_splits_4, v_splits, 0.01, split_type=3)
	# print(t_splits_4, splits, mlss[-1]) 
	# mlss = tandem_queue.MLSS(T, V, splits_5, t_splits, v_splits, 0.01, split_type=3)
	# print(t_splits, splits_5, mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits_6, t_splits, v_splits, 0.01, split_type=3)
	# print(t_splits, splits_6, mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits_7, t_splits, v_splits, 0.01, split_type=3)
	# print(t_splits, splits_7, mlss[-1])
	# mlss = tandem_queue.MLSS(T, V, splits_8, t_splits, v_splits, 0.01, split_type=3)
	# print(t_splits, splits_8, mlss[-1])

if __name__ == '__main__':
	# tandem_queue_model_test()
	# tandem_queue_split_by_time_test()
	tandem_queue_hybrid_test()




