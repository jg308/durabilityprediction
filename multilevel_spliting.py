import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import random
import copy
from collections import defaultdict

SIGMA = 1
Constant = 5
AR_0 = 5
AR_1 = 5
CE_batch = 10000
Phi0 = 0.5
Phi1 = 0.9
N = 1000 
K = 100


def white_noises(sigma, T):
    return np.random.normal(0, sigma, T)

def poisson_arrival(T, lamda):
	arrival_times = set()
	_arrival_time = 0
	while True:
		#Get the next probability value from Uniform(0,1)
		p = np.random.uniform(0,1,1)[0]

		#Plug it into the inverse of the CDF of Exponential(_lamnbda)
		_inter_arrival_time = -np.log(p)/lamda

		#Add the inter-arrival time to the running sum
		_arrival_time = _arrival_time + _inter_arrival_time

		arrival_times.add(int(_arrival_time))

		if _arrival_time > T:
			break

	return arrival_times

def tandem_queue(queue_1, queue_2, T, lamda, mu_1, mu_2, level, plot=False):

	arrival_instants = poisson_arrival(T, lamda)
	#print(sorted(arrival_instants))
	q1_sequence = []
	q2_sequence = []

	deque_1_time = int(np.random.exponential(mu_1,1)[0])+1
	deque_2_time = int(np.random.exponential(mu_2,1)[0])+1

	for t in range(T):
		
		# join queue 1
		if t in arrival_instants:
			queue_1 += 1
		if queue_1 == 0:
			deque_1_time += 1
		# quit queue 1 and join queue 2
		if queue_1 > 0 and t == deque_1_time:
			queue_1 -= 1
			queue_2 += 1
			deque_1_time += int(np.random.exponential(mu_1,1)[0])+1
		# quit queue 2
		if queue_2 == 0:
			deque_2_time += 1
		if queue_2 > 0 and t == deque_2_time:
			queue_2 -= 1
			deque_2_time += int(np.random.exponential(mu_2,1)[0])+1

		q1_sequence.append(queue_1)
		q2_sequence.append(queue_2)

		if queue_2 >= level:
			break
		# print(t, queue_1, queue_2, deque_1_time, deque_2_time)
		# stop condition
		# if queue_1 == 0 and queue_2 == 0:
		# 	break

	# print(q1_sequence[:20])
	# print(q2_sequence[:20])
	if plot:
		print(q2_sequence)
		#plt.plot(q1_sequence, color='r', label='Queue 1')
		plt.plot(q2_sequence, color='black', label='Queue 2')
		plt.hlines(level, 0, T, color='r')
		plt.legend()
		plt.show()

	return q1_sequence, q2_sequence

# actually AR1
def markovian_trace(input_0, errors, boundary = None):
	sequence = []
	x0 = input_0
	for i in range(len(errors)):
		#print(x0, x1)
		x1 = Constant + Phi0 * x0 + errors[i]
		sequence.append(x1)
		if boundary is not None and x1 >= boundary:
			break
		x0 = x1
	return sequence

def simple_random_sampling_with_cost(sample_size, T, level, true_prob, verbose=False):
	samples = []
	total_cost = 0
	for i in range(sample_size):
		# epsilon = white_noises(SIGMA, T)
		# sequence = markovian_trace(AR_0, epsilon, level)
		_, sequence = tandem_queue(0, 0, T, 0.5, 2, 2, level)
		total_cost += len(sequence)
		if len(sequence) < T:
			samples.append(1)
		else:
			samples.append(0)
	mean = np.mean(samples)
	if mean == 0:
		# undefined relative error
		ci = 1
		relative_error = 1000
	else:
		ci = 1.96 * np.sqrt(np.var(samples) / len(samples))
		relative_error = np.sqrt(np.var(samples)) / true_prob
	if verbose:
		print('estimate:{}, ci:{}, RE:{}, cost:{}'.format(mean, ci, relative_error, total_cost))
	return mean, relative_error, total_cost

def multilevel_splitting_cost(batch_size, split_levels, T, level, ground_truth, verbose=False):
	colors = ['r','b']
	total_path = 0
	total_cost = 0
	previous_states = {}
	level_prob = []
	valid_cnt = 0
	root_success = defaultdict(int)
	for idx, bound in enumerate(split_levels):
		#print(idx)
		#plt.hlines(bound, 0, T)
		#print(previous_states)
		level_cost = 0
		current_states = []
		if idx > 0:
			c = batch_size // valid_cnt
			d = batch_size % valid_cnt
			selected_keys = np.random.choice(list(previous_states.keys()), size=d, replace=False)
			for key in selected_keys:
				for i in range(c+1):
					current_states.append(previous_states[key])
			for key in previous_states:
				if key not in selected_keys:
					for i in range(c):
						current_states.append(previous_states[key])
		else:
			current_states = [(0, 0, 0, -1)] * batch_size

		# fixed-effort
		# assert len(current_states) == batch_size
		#print(current_states)
		previous_states.clear()
		trace_id = 0
		valid_cnt = 0
		for state in current_states:
			q1, q2, length, root = state
			sequence1, sequence2 = tandem_queue(q1, q2, T - length, 0.5, 2, 2, bound)
			total_cost += len(sequence2)
			level_cost += len(sequence2)
			trace_id += 1
			if len(sequence2) + length < T:
				# assert sequence2[-1] >= bound
				if root == -1:
					root = trace_id
				previous_states[trace_id] = (sequence1[-1], sequence2[-1], len(sequence2) + length, root)
				valid_cnt += 1
				if bound == level:
					root_success[root] += 1
					total_path += 1
			else:
				total_path += 1
			#print(state)
			#plt.plot(range(length, length + len(sequence2)), sequence2, color=colors[idx%2])
		level_prob.append((valid_cnt, batch_size))
		if verbose:
			print('level:{}, prob:{}, level_cost:{}'.format(\
				bound, level_prob[-1], level_cost))
	
	relative_error = np.sqrt(np.sum([(1 - item[0] / item[1])/(item[0]) for item in level_prob]))
	#relative_error = np.sqrt(np.prod([(1 - item[0] / item[1])/item[0] + 1 for item in level_prob]) - 1)

	total_prob = 1
	for p  in level_prob:
		total_prob *= p[0] / p[1]

	#plt.show()
	if verbose:
		print('estimate:{}, RE:{}, cost:{}, total paths:{}, avg path cost:{}'.format(\
			total_prob, relative_error, total_cost, total_path, total_cost / total_path))

	return total_prob, relative_error, total_cost, total_path

def unbiased_multilevel_splitting_cost(batch_size, split_levels, T, level, ground_truth, verbose=False):
	total_path = 0
	total_cost = 0
	# key : entrance timestamp; value : list of entrance states
	previous_states = defaultdict(list)
	state_prob = defaultdict(float)
	last_state_prob = defaultdict(float)

	for idx, bound in enumerate(split_levels):
		level_cost = 0
		current_states = []
		# initial state
		if idx == 0:
			# state: (queue-1 size, queue-2 size, timestamp)
			current_states = [(0,0,0)] * batch_size
			last_state_prob[0] = 1
		# start from previous states
		else:
			last_state_prob = copy.deepcopy(state_prob)
			# construct CDF
			time_cdf = []
			time_state = sorted(previous_states.keys())
			cutoff = 0
			for time in time_state:
				cutoff += len(previous_states[time])
				time_cdf.append(cutoff)

			# sampling from emprical distribution
			seeds = np.random.uniform(1, cutoff, batch_size)
			for s in seeds:
				loc = list(filter(lambda i : i<=s, time_cdf))
				if len(loc) == 0:
					time_idx = 0
				else:
					timestamp = loc[-1]
					time_idx = time_cdf.index(timestamp)
				current_states.append(random.choice(previous_states[time_state[time_idx]]))

		# simulation
		assert len(current_states) == batch_size
		previous_states.clear()
		state_prob.clear()
		
		#print('last level probs:')
		#print(last_state_prob)
		valid_path = 0
		for state in current_states:

			q1,q2,time = state
			sequence1, sequence2 = tandem_queue(q1, q2, T-time, 0.5, 2,2, bound)
			total_cost += len(sequence1) + len(sequence2)
			level_cost += len(sequence1) + len(sequence2)
			new_time = len(sequence2) + time
			if new_time < T or (new_time == T and sequence2[-1] >= bound):
				assert sequence2[-1] >= bound
				previous_states[new_time].append((sequence1[-1], sequence2[-1], new_time))
				state_prob[new_time] += last_state_prob[time] * 1 # 1 / number of paths generated from time
				valid_path += 1

		# update level entrance probabilities
		# for t in state_prob:
		# 	state_prob[t] = state_prob[t] / batch_size

		
		#print('current level probs:')
		#print(state_prob)
		if verbose:
			print('==level:{}, bound:{}=='.format(idx, bound))
			for time in sorted(previous_states.keys()):
				print(time, len(previous_states[time]))

	# unbiased estimation
	estimator = np.sum([state_prob[t] for t in state_prob])
	print(estimator)


def level_splitting_cost(batch_size, split_levels, T, level, ground_truth):
	colors = ['r','b']
	total_cost = 0
	previous_states = {}
	level_prob = []
	valid_cnt = 0
	root_success = defaultdict(int)
	for idx, bound in enumerate(split_levels):
		
		#plt.hlines(bound, 0, T)
		#print(previous_states)
		level_cost = 0
		current_states = []
		if idx > 0:
			c = batch_size // valid_cnt
			d = batch_size % valid_cnt
			selected_keys = np.random.choice(list(previous_states.keys()), size=d, replace=False)
			for key in selected_keys:
				for i in range(c+1):
					current_states.append(previous_states[key])
			for key in previous_states:
				if key not in selected_keys:
					for i in range(c):
						current_states.append(previous_states[key])
		else:
			current_states = [(AR_0, 0, -1)] * batch_size

		# fixed-effort
		assert len(current_states) == batch_size
		#print(current_states)
		previous_states.clear()
		trace_id = 0
		valid_cnt = 0
		for state in current_states:
			value, length, root = state
			#print(value, length)
			sequence = markovian_trace(value, white_noises(SIGMA, T - length), bound)
			total_cost += len(sequence)
			level_cost += len(sequence)
			trace_id += 1
			if len(sequence) + length < T:
				assert sequence[-1] >= bound
				if root == -1:
					root = trace_id
				previous_states[trace_id] = (sequence[-1], len(sequence) + length, root)
				valid_cnt += 1
				if bound == level:
					root_success[root] += 1
			#plt.plot(range(length, length + len(sequence)), sequence, color=colors[idx%2])
		level_prob.append((valid_cnt, batch_size))
		print('level:{}, prob:{}, level_cost:{}'.format(bound, level_prob[-1], level_cost))
	
	#relative_error = np.sqrt(np.sum([(1 - item[0] / item[1])/(item[0]) for item in level_prob]))
	relative_error = np.sqrt(np.prod([(1 - item[0] / item[1])/item[0] + 1 for item in level_prob]) - 1)

	total_prob = 1
	for p  in level_prob:
		total_prob *= p[0] / p[1]

	#plt.show()
	print('estimate:{}, RE:{}, cost:{}'.format(total_prob, relative_error, total_cost))

def simple_random_sampling(ci_threshold, batch_size, T, level, plot_id = None):
	samples = []
	mean = 0
	ci = 1
	global_id = 0
	while ci > ci_threshold or ci == 0:
		for i in range(batch_size):
			if i % 1000 == 0:
				print('\r {}'.format(i), end = '')
			# epsilon = white_noises(SIGMA, T)
			# sequence = markovian_trace(AR_0, epsilon, level)
			_, sequence = tandem_queue(0, 0, T, 0.5, 2, 2, level)
			global_id += 1
			if plot_id is not None and global_id == plot_id:
				plt.plot(sequence)
				plt.show()
			if len(sequence) < T:
				samples.append(1)
			else:
				samples.append(0)
		mean = np.mean(samples)
		ci = 1.96 * np.sqrt(np.var(samples) / len(samples))
		print(mean, ci)
	print(mean, ci)
	print(np.var(samples) / mean)
	print('sample count:{}'.format(len(samples)))
	return mean

def numerical_test(prob, cost):
	srs = np.sqrt((1-prob)/(cost*prob))
	splitting = -(2.71828 * np.log(prob))/(2*np.sqrt(cost))
	return srs, splitting

def sampling_comparison(cost):
	var_srs = []
	var_split = []
	for p in np.linspace(0,1,1000):
		if p > 0:
			a,b = numerical_test(p, cost)
			var_srs.append(a)
			var_split.append(b)

	plt.plot(np.linspace(0,1,1000)[1:],var_srs, color='b', label='Simple Random Sampling')
	plt.plot(np.linspace(0,1,1000)[1:],var_split, color='r', label='Splitting')
	plt.ylabel('Relative Error')
	plt.xlabel('Probability')
	plt.legend()
	plt.xlim(0, 0.2)
	plt.show()

def proof_of_concept_test(timeframe, boundary, ground_truth):
	# simple random sampling with cost
	print('===SRS Cost===')
	simple_random_sampling_with_cost(1000, timeframe, boundary, ground_truth, True)

	# splitting
	print('===Splitting Cost===')
	multilevel_splitting_cost(800, [6, 8, 10, boundary], timeframe, boundary, ground_truth, True)

def cost_comparison(timeframe, boundary, ground_truth):
	srs_costs = []
	for sample_size in range(500, 10000, 500):
		print('srs:{}'.format(sample_size))
		_,_, cost = simple_random_sampling_with_cost(sample_size, timeframe, boundary, ground_truth, False)
		srs_costs.append(cost)

	mls_cost_2 = []
	mls_path_2 = []
	mls_cost_3 = []
	mls_path_3 = []
	mls_cost_4 = []
	mls_path_4 = []

	for sample_size in range(200, 6000, 200):
		print('mls:{}'.format(sample_size))
		_,_, cost_2, paths_2 = multilevel_splitting_cost(sample_size, \
			[boundary//2, boundary], timeframe, boundary, ground_truth, False)
		mls_cost_2.append(cost_2)
		mls_path_2.append(paths_2)
		_,_, cost_3, paths_3 = multilevel_splitting_cost(sample_size, \
			[boundary//3, 2*boundary//3, boundary], timeframe, boundary, ground_truth, False)
		mls_cost_3.append(cost_3)
		mls_path_3.append(paths_3)
		_,_, cost_4, paths_4 = multilevel_splitting_cost(sample_size, \
			[boundary//4, 2*boundary//4, 3*boundary//4, boundary], timeframe, boundary, ground_truth, False)
		mls_cost_4.append(cost_4)
		mls_path_4.append(paths_4)

	plt.plot(range(500, 10000, 500), srs_costs, label='SRS')
	plt.plot(mls_path_2, mls_cost_2, label='MLS-2')
	plt.plot(mls_path_3, mls_cost_3, label='MLS-3')
	plt.plot(mls_path_4, mls_cost_4, label='MLS-4')
	plt.legend()
	plt.show()

def empirical_comparison(timeframe, boundary, ground_truth):

	max_sample = 5000
	trail_size = 10

	SRS_err = []
	MLS_err = []
	SRS_cost = []
	MLS_cost = []
	SRS_estimate = []
	MLS_estimate = []
	for sample_size in range(100, max_sample, 200):
		print('sample size:{}'.format(sample_size))
		srs_trails = []
		mls_trails = []
		for j in range(trail_size):
			srs_e, srs_re, srs_c = simple_random_sampling_with_cost(sample_size, timeframe, boundary, ground_truth)
			mls_e, mls_re, mls_c, _ = multilevel_splitting_cost(sample_size//2, [6, boundary], timeframe, boundary, ground_truth)
			srs_trails.append((srs_re, srs_c, srs_e))
			mls_trails.append((mls_re, mls_c, mls_e))

		srs_trail_cost = [item[1] for item in srs_trails]
		srs_trail_err = [item[0] for item in srs_trails]
		srs_trail_est = [item[2] for item in srs_trails]
		mls_trail_cost = [item[1] for item in mls_trails]
		mls_trail_err = [item[0] for item in mls_trails]
		mls_trail_est = [item[2] for item in mls_trails]
		

		SRS_err.append((np.max(srs_trail_err), np.mean(srs_trail_err), np.min(srs_trail_err)))
		SRS_cost.append(np.mean(srs_trail_cost))
		SRS_estimate.append((np.max(srs_trail_est), np.mean(srs_trail_est), np.min(srs_trail_est)))
		MLS_err.append((np.max(mls_trail_err), np.mean(mls_trail_err), np.min(mls_trail_err)))
		MLS_cost.append(np.mean(mls_trail_cost))
		MLS_estimate.append((np.max(mls_trail_est), np.mean(mls_trail_est), np.min(mls_trail_est)))

	print(SRS_err)
	print(SRS_cost)
	print(MLS_err)
	print(MLS_cost)
	# Relative Error PLOT
	plt.subplot()

	start_idx = 0
	for idx, item in enumerate(SRS_err):
		if item[0] > 10:
			start_idx = idx+1

	plt.plot(SRS_cost[start_idx:], [item[1] for item in SRS_err[start_idx:]], '-', color='b', \
		label='Simple Random Sampling')
	plt.fill_between(SRS_cost[start_idx:], [item[2] for item in SRS_err[start_idx:]], [item[0] for item in SRS_err[start_idx:]], alpha = 0.2, color='b')

	plt.plot(MLS_cost, [item[1] for item in MLS_err], '-', color='r', \
		label='Multi-Level Splitting')
	plt.fill_between(MLS_cost, [item[2] for item in MLS_err], [item[0] for item in MLS_err], alpha= 0.2, color ='r')

	plt.xlabel('simulation steps')
	plt.ylabel('relative error')
	plt.legend()
	plt.show()

	# Esimate plot
	plt.subplot()
	plt.plot(SRS_cost, [item[1] for item in SRS_estimate], '-', color='b', label='Simple Random Sampling')
	plt.fill_between(SRS_cost, [item[2] for item in SRS_estimate], [item[0] for item in SRS_estimate], alpha = 0.2, color = 'b')

	plt.plot(MLS_cost, [item[1] for item in MLS_estimate], '-', color='r', label='Multi-Level Splitting')
	plt.fill_between(MLS_cost, [item[2] for item in MLS_estimate], [item[0] for item in MLS_estimate], alpha = 0.2, color = 'r')

	plt.hlines(ground_truth, 0, max(SRS_cost), color='black', label='ground truth')

	plt.xlabel('simulation steps')
	plt.ylabel('esimate')
	plt.legend()
	plt.show()



if __name__ == '__main__':

	# 200-20: 0.0136909090909
	# 200-8 : 0.5612
	timeframe = 100
	boundary = 8
	# tandem_queue(0, 0, timeframe, 0.5, 2, 2, boundary, plot=True)
	# ground truth
	#ground_truth = simple_random_sampling(0.1, 5000, timeframe, boundary, plot_id=None)
	# ground_truth = 0.5612

	# empirical_comparison(timeframe, boundary, ground_truth)
	# proof_of_concept_test(timeframe, boundary, ground_truth)
	# cost_comparison(timeframe, boundary, ground_truth)
	#unbiased_multilevel_splitting_cost(1000, [10, 20], timeframe, boundary, ground_truth, verbose=False)

