import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from bisect import bisect_left
from collections import Counter, defaultdict
import seaborn as sns


feature_size = 50
layer1 = 50
layer2 = 20

phi = 0.3
sigma = 2
initial = 0

# Modeling Definition
def NN2(X_train, Y_train, X_test, Y_test, learning_rate = 0.001, iterations=10000):
    tf.reset_default_graph()   
    
    #create placeholder
    X = tf.placeholder(tf.float32, [feature_size, None], name="X")
    Y = tf.placeholder(tf.float32, [1, None], name="Y")
    keep_prob = tf.placeholder(tf.float32)

    #create variable
    #50 nodes in the first hidden layer
    #20 nodes in the second hidden layer
    W1 = tf.get_variable("W1", [layer1, feature_size], initializer = tf.contrib.layers.xavier_initializer())
    b1 = tf.get_variable("b1", [layer1, 1], initializer = tf.zeros_initializer())
    W2 = tf.get_variable("W2", [layer2, layer1], initializer = tf.contrib.layers.xavier_initializer())
    b2 = tf.get_variable("b2", [layer2, 1], initializer = tf.zeros_initializer())
    W3 = tf.get_variable("W3", [1, layer2], initializer = tf.contrib.layers.xavier_initializer())
    b3 = tf.get_variable("b3", [1, 1], initializer = tf.zeros_initializer())

    params = {
        'W1':W1,
        'b1':b1,
        'W2':W2,
        'b2':b2,
        'W3':W3,
        'b3':b3
    }
    #forward
    Z = tf.add(tf.matmul(W1, X), b1)
    Z = tf.nn.relu(Z)
    Z = tf.nn.dropout(Z, keep_prob)
    Z = tf.add(tf.matmul(W2, Z), b2)
    Z = tf.nn.relu(Z)
    Z = tf.nn.dropout(Z, keep_prob)
    A = tf.add(tf.matmul(W3, Z), b3)

    #loss
    cost = tf.reduce_mean(tf.squared_difference(A, Y))

    #back
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        for i in range(iterations):
            _ , itr_cost = sess.run([optimizer, cost], feed_dict={X: X_train, Y: Y_train, keep_prob: 0.8})
            if i % 100 == 0:
                #validation
                validation_cost = sess.run(cost, feed_dict={X: X_test, Y: Y_test, keep_prob: 1.0})
                print ("iteration {}; training loss: {}, validation loss: {}"\
                	.format(i, itr_cost, validation_cost))
        training_output = sess.run(A, feed_dict={X: X_train, keep_prob: 1.0})
        test_output = sess.run(A, feed_dict={X:X_test, keep_prob: 1.0})
    return training_output, test_output

def ar1_model(x, phi, sigma, error):
	return phi * x + error

def random_walk_model(x, error):
	return x + error

def simulation_prediction(model, trails, boundary, limit=300):
	X = []
	Y = []
	
	FHT = []

	for i in range(trails):
		length = 0
		values = []
		x = initial
		error = np.random.normal(0, sigma, limit)
		while x < boundary and length < limit:
			if model is 'ar1':
				x = ar1_model(x, phi, sigma, error[length])
			elif model is 'rw':
				x = random_walk_model(x, error[length])
			else:
				print('wrong model paras')
			values.append(x)
			length+=1
		prefix = np.zeros(feature_size)
		if feature_size > len(values):
			prefix[0:len(values)] = values
		else:
			prefix = values[0:feature_size]
		X.append(prefix)
		Y.append(length)
		FHT.append(length)

	X_train = X[:1000]
	Y_train = Y[:1000]
	X_test =  X[-1000:]
	Y_test = Y[-1000:]

	X_train = np.transpose(np.array(X_train))
	Y_train = np.array(Y_train).reshape(1, len(Y_train))
	X_test = np.transpose(np.array(X_test))
	Y_test = np.array(Y_test).reshape(1, len(Y_test))

	#print('estimate : {} / {}'.format(np.mean(FHT[:1000]), np.var(FHT[:1000])/1000))

	mean = np.mean(FHT)
	var = np.var(FHT) / len(FHT)

	return X_train, Y_train, X_test, Y_test, mean, var

def simple_post_stratification(X_train, Y_train):
	paths = np.transpose(X_train)
	label = list(Y_train[0])
	strata = defaultdict(list)
	paths.tolist()
	
	for idx,path in enumerate(paths):
		if path[0] >= initial:
			strata[1].append(label[idx])
		else:
			strata[0].append(label[idx])

	print(len(strata[0]), len(strata[1]))

	adjusted_mean = np.mean(strata[0]) * 0.5 + np.mean(strata[1]) * 0.5
	size = len(paths)
	var0 = np.var(strata[0]) / len(strata[0])
	var1 = np.var(strata[1]) / len(strata[1]) 
	adjusted_var = 1/size * 0.5*(var0 + var1) + 1/(size**2) * 0.5*(var0 + var1)

	#print('post stratification mean: {} / {}'.format(adjusted_mean, adjusted_var))

	return adjusted_mean, adjusted_var

def post_stratification():
	srs_mean = []
	ps_mean = []
	for i in range(100):
		X,Y,srs,_ = simulation_prediction('ar1', 100, 5)
		srs_mean.append(srs)
		ps,_ = simple_post_stratification(X, Y)
		ps_mean.append(ps)

	print('srs estimate : {}/{}'.format(np.mean(srs_mean), np.var(srs_mean)/len(srs_mean)))
	print('ps estimate : {}/{}'.format(np.mean(ps_mean), np.var(ps_mean)/len(ps_mean)))

# def csf_stratification(cnt_bin, number_of_strata):
#     sqrt_f = []
#     counter = sorted(cnt_bin.keys())
#     for key in counter:
#         sqrt_f.append(np.sqrt(cnt_bin[key]))
#     accumulated_f = np.cumsum(sqrt_f)
#     bar = accumulated_f[-1] / number_of_strata
    
#     partition = []
    
#     k = 1
#     for i in range(len(accumulated_f)):
#         if i == len(accumulated_f)-1 or k == number_of_strata:
#             partition.append(counter[i])
#             break
#         if accumulated_f[i] > k*bar:
#             partition.append(counter[i])
#             k+=1
#     return partition


# def post_stratification_csf(Y_predict, number_of_strata):
# 	cnt_bin = Counter(list(Y_predict[0]))
# 	partitions = csf_stratification(cnt_bin, number_of_strata)
# 	return partitions

# def find(a, x):
#     'Find leftmost index such that the value is greater than or equal to x'
#     i = bisect_left(a, x)
#     if i != len(a):
#         return i
#     else:
#         return i-1

# def get_post_weight(Y_predict, partition):
# 	weights = defaultdict(int)
# 	for t in list(Y_predict[0]):
# 		idx = find(partition, t)
# 		weights[idx] += 1
# 	return weights

if __name__ == '__main__':
	X_train, Y_train, X_test, Y_test, ground_mean, ground_var = simulation_prediction('ar1', 2000, 5)
	print('estimate: {}'.format(ground_mean))
	train_output, test_output = NN2(X_train, Y_train, X_test, Y_test)
	plt.subplot(221)
	sns.distplot(list(Y_train), hist=False, kde=True)
	sns.distplot(list(train_output), hist=False, kde=True)
	plt.title('Training')
	plt.subplot(222)
	sns.distplot(list(train_output - Y_train))
	plt.title('Training Residual')
	plt.subplot(223)
	sns.distplot(list(Y_test), hist=False, kde=True)
	sns.distplot(list(test_output), hist=False, kde=True)
	plt.title('Validation')
	plt.subplot(224)
	sns.distplot(list(test_output - Y_test))
	plt.title('Validation Residual')
	plt.tight_layout()
	plt.show()



